<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->


      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.min.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!-- <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all"> -->
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">

    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/abennington/pen/GZeyKr?depth=everything&order=popularity&page=45&q=pack&show_forks=false" />

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'><link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
  <style class="cp-pen-styles">img { max-height: 100% }
  .swiper-container {
    width: 100%;
    height: 400px;
  }

  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  /*width:90%;*/
  }
  /* carousel */
  .media-carousel
  {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
    margin-top: 30px;
  }
  /* Previous button  */
  .media-carousel .carousel-control.left
  {
    left: -12px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Next button  */
  .media-carousel .carousel-control.right
  {
    right: -12px !important;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Changes the position of the indicators */
  .media-carousel .carousel-indicators
  {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
  }
  /* Changes the colour of the indicators */
  .media-carousel .carousel-indicators li
  {
    background: #c0c0c0;
  }
  .media-carousel .carousel-indicators .active
  {
    background: #333333;
  }
  .media-carousel img
  {
    width: 250px;
    height: 100px
  }
  /* End carousel */

</style>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: Lato;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #2c3e50;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }

        .pb-video-frame:hover {
            height: 300px;
        }

    .pb-row {
        margin-bottom: 10px;
    }
</style>
</head>
   <body>
     <section class="news py-5" id="services">
      <div class="container py-lg-5">
        <div class="row blog_w3l_top">
          <div class="col-lg-12 blog_w3l_right">
            <h2 class="heading text-center mb-5"  style="font-size:30px;text-align:center;"> <strong>JANGAN BIARKAN PENUAAN DINI MENIMPA DIRI ANDA
LEBIH BAIK MENCEGAH DARIPADA MENGOBATI</strong>
     </h2>
              <center><img src="{{asset('web/images/aluze/aluze1.png')}}" alt="news image" class="img-fluid"></center>
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>Apa itu penyakit penuaan kulit (penuaan dini)?</h6>
                   <p style="text-align:justify;font-size:20px;font-size:20px;">Kulit telah terbukti rentan terhadap berbagai faktor seiring bertambahnya usia, seperti sinar matahari, cuaca tidak menentu, dan kebiasaan buruk. Kondisi-kondisi itu dapat berkontribusi terhadap penuaan kulit (skin aging). Meski begitu, tetap tersedia beberapa cara untuk menjaganya tetap sehat dan segar.</p>
                   <p style="text-align:justify;font-size:20px;">Penuaan kulit dipicu oleh berbagai faktor, seperti gaya hidup, pola makan, keturunan, dan kebiasaan pribadi lain. Sebagai contoh, merokok dapat menghasilkan radikal bebas. Radikal bebas terbentuk dari molekul oksigen yang sebelumnya sehat, kini menjadi terlampau aktif dan tidak stabil. Kemampuan sel-sel tubuh untuk menghancurkan radikal bebas dapat menyebabkan penuaan dini.</p>
                   <p style="text-align:justify;font-size:20px;">Faktor lain yang berkontribusi terhadap penuaan kulit meliputi stres, gravitasi, pergerakan wajah, obesitas, dan bahkan posisi tidur.</p>

                 </div>

              </div>
          </div>


          </div>
        </div>
     </section>
     <section class="services py-5" id="services">
      <div class="container py-lg-5">
        <div class="row blog_w3l_top">
          <div class="col-lg-12 blog_w3l_right">
            <h2 class="heading text-center mb-5"> <strong>TANDA –TANDA PENUAAN DINI MENURUT PARA AHLI</strong></h2>
              <center><img src="{{asset('web/images/aluze/aluze2.png')}}" alt="news image" class="img-fluid"></center>
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>1. Bintik hitam akibat paparan sinar matahari di usia 20 atau 30-an</h6>
                   <p style="text-align:justify;font-size:20px;">Bintik-bintik hitam yang timbul di permukaan kulit wajah, akibat terpapar sinar matahari di akhir usia 20 atau awal 30-an harus menjadi peringatan pertama bagi Anda.</p>
                  <br>  <h6>2. Tangan yang kurus</h6>
                   <p style="text-align:justify;font-size:20px;">Kehilangan jaringan pada tulang dapat membuat tangan dan wajah Anda menjadi tirus, ditambah dengan kantong di bawah mata yang semakin membesar. Proses ini sangat bertahap, sehingga kebanyakan orang tidak menyadarinya, kecuali Anda memotret wajah secara berkala.</p>
                   <br><h6>3. Kulit di leher berbeda warna dan mudah iritasi</h6>
                   <p style="text-align:justify;font-size:20px;">Warna yang tidak rata di bagian leher dan daerah V, serta mudahnya kulit teriritasi adalah tanda-tanda penuaan. Perlu diketahui bahwa kulit yang sehat memiliki penghalang yang kuat untuk peradangan kronis.</p>
                   <br><h6>4. Kerutan di sepanjang pipi dan rahang</h6>
                   <p style="text-align:justify;font-size:20px;">Jika Anda mendapati garis-garis halus dan kerutan muncul di tengah-tengah pipi dan rahang saat usia 20 atau 30an, ini adalah tanda-tanda penuaan yang buruk.</p>
                   <br><h6>5. Kulit Anda lebih kasar dari biasanya</h6>
                   <p style="text-align:justify;font-size:20px;">Tanda-tanda penuaan lainnya adalah hilangnya kelembapan kulit, karena membran sel menjadi lebih berpori, dan inilah yang menyebabkannya kering. Akibatnya, kulit terasa gatal dan lebih sensitif.</p>
                   <br><h6>6. Mata Anda berubah bentuk</h6>
                   <p style="text-align:justify;font-size:20px;">Seiring bertambahnya usia, tulang manusia pun akan semakin menjorok ke dalam. Area yang akan paling menonjol adalah sudut dalam atas dan sudut luar bawah tengkorak, seperti mata. Hasilnya, mata yang tadinya berbentuk bulat lama kelamaan akan terlihat horisontal.</p>

                 </div>

              </div>
          </div>

          <!-- <div class="col-lg-4 blog_w3l_left"> -->
            <!-- <div class="blog_w3l_left1">
              <div class="blog_w3l-2">
                <h5>KONSULTASI VIA WHATSAPP</h5><br>
                <a href="https://api.whatsapp.com/send?phone=6282312543008&text=Halo%20Admin%20Saya%20Mau%20Tanya">  <img src="{{asset('web/images/button_whatsapp.png')}}" class="responsive" width="80%"/></a> -->
                <!-- <h3 class="mt-2">pellentesque mi non</h3> -->
                <!-- <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p> -->
              <!-- </div>
            </div> -->
            <!-- <div class="blog_w3l_left2">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
            <!-- <div class="blog_w3l_left3">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
          <!-- </div> -->
          </div>
        </div>
     </section>
     <section class="news py-5" id="services">
      <div class="container py-lg-5">
        <div class="row blog_w3l_top">
          <div class="col-lg-12 blog_w3l_right">
            <h2 class="heading text-center mb-5"> <strong>Bagaimana cara alami mengatasi penuaan kulit (skin aging)?</strong></h2>
          </div>
          <div class="col-lg-4 blog_w3l_right">
            <center><img src="{{asset('web/images/aluze/aluze2.png')}}" alt="news image" class="img-fluid"></center>
          </div>
          <div class="col-lg-8 blog_w3l_right">

              <!-- <center><img src="{{asset('web/images/aluze/aluze2.png')}}" alt="news image" class="img-fluid"></center> -->
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
                       <li>Lindungi kulit dari sinar matahari setiap hari</li>
                       <li>Gunakan <strong>self-tanner</strong>  dibandingkan dengan berjemur</li>
                       <li>Berhenti merokok</li>
                       <li>Hindari ekspresi wajah yang berulang</li>
                       <li>Pola makan yang seimbang</li>
                       <li>Berhenti konsumsi alkohol</li>
                       <li>Berolahraga rutin</li>
                       <li>Hentikan penggunaan produk perawatan kulit yang menyebabkan perih</li>
                   </ol>

                 </div>

              </div>
          </div>

          <!-- <div class="col-lg-4 blog_w3l_left"> -->
            <!-- <div class="blog_w3l_left1">
              <div class="blog_w3l-2">
                <h5>KONSULTASI VIA WHATSAPP</h5><br>
                <a href="https://api.whatsapp.com/send?phone=6282312543008&text=Halo%20Admin%20Saya%20Mau%20Tanya">  <img src="{{asset('web/images/button_whatsapp.png')}}" class="responsive" width="80%"/></a> -->
                <!-- <h3 class="mt-2">pellentesque mi non</h3> -->
                <!-- <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p> -->
              <!-- </div>
            </div> -->
            <!-- <div class="blog_w3l_left2">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
            <!-- <div class="blog_w3l_left3">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
          <!-- </div> -->
          </div>
        </div>
     </section>
     <section class="ban_bottom1 py-5" id="more">
     	<div class="container py-lg-5">
     		<div class="ban_bottom_top text-center py-lg-5">
     			<h3>Nah, ada solusi praktis & alami tanpa bahan-bahan kimia yang bisa membantu anda mengatasi masalah penuaan dini, yaitu Glutathione</h3>
     		</div>
     	</div>
     </section>
     <section class="news py-5" id="services">
      <div class="container py-lg-5">
        <div class="row blog_w3l_top">
          <div class="col-lg-12 blog_w3l_right">
            <h2 class="heading text-center mb-5"> <strong>Apa Itu Glutathione dan Manfaatnya ?</strong></h2>
          </div>
          <div class="col-lg-6 blog_w3l_right">
            <div class="blog_w3l-5">
               <div class="blog_w3l-8">
            <!-- <h6> <strong>Apa Itu Glutathione dan Manfaatnya ?</strong></h6> -->
            <h6>1. Sebagai Antioksidan Alami</h6>
            <p style="text-align:justify;font-size:20px;">Pada dasarnya, tubuh manusia dapat menetralisir radikal bebas karena tubuh manusia memproduksi antioksidan alamiah yaitu Glutathione (GSH).
Dengan kerusakan yang diakibatkan radikal bebas dan proses penuaan, jumlah Glutathione (GSH) menjadi semakin menurun. Oleh karena itu, diperlukan Precursors (bahan baku utama) untuk produksi Glutathione (GSH).
Glutathione (GSH) disebut sebagai Master Antioksidan karena telah diakui oleh para ahli medis internasional bahwa belum ada antioksidan lain yang mampu menyamai kekuatannya karena banyak fungsi sel-sel tubuh yang tergantung pada Glutathione (GSH). Bahkan, saat ini tak kurang 100.000 jurnal medis yang telah membahasnya.</p>
<br><h6>2. Membantu Proses Regenerasi Sel</h6>
<p style="text-align:justify;font-size:20px;">Glutathione (GSH) mengatur metabolisme tubuh, memisahkan & menghancurkan radikal bebas, membuat Glutathione (GSH) aktif untuk merangsang regenerasi sel baru bagi tubuh.</p>
<br><h6>3. Sebagai Faktor Anti-Aging</h6>
<p style="text-align:justify;font-size:20px;">Glutathione (GSH) dapat membantu meningkatkan kandungan kolagen dalam tubuh kita terutama kulit, sehingga dapat mengurangi kerutan-kerutan pada wajah dan menghambat penuaan. Tepat kalau disebut sebagai faktor anti-aging.</p>

          </div></div></div>
          <div class="col-lg-6 blog_w3l_right">

              <!-- <center><img src="{{asset('web/images/aluze/aluze2.png')}}" alt="news image" class="img-fluid"></center> -->
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>Sumber Utama Dan Cara Kerja Glutathione</h6>
                   <p style="text-align:justify;font-size:20px;">Sumber utama Glutathione adalah bahan alami organik seperti asparagus, brokoli, alpukat, dan bayam. Selain itu, semua buah-buahan mentah dan sayuran segar sangat baik untuk meningkatkan tingkat Glutathione, khususnya sayuran seperti kembang kol, kubis, dan taoge. Sumber Glutathione lainnya antara lain telur, daging segar, bawang putih, dan kunyit.</p>
                   <br><h6>Bagaimana Cara Kerja Glutathione ?</h6>
                   <p style="text-align:justify;font-size:20px;">Kebanyakan antioksidan bekerja di jaringan tertentu. Hal tersebut berarti mereka cenderung berkonsentrasi di satu atau dua tempat dalam tubuh, dan di sanalah mereka memberikan perlindungan antioksidan.</p>
                   <p style="text-align:justify;font-size:20px;">Glutathione (GSH) bersifat spesial karena aktif di banyak bagian tubuh sehingga menjadi perisai antioksidan yang lebih lengkap.</p>
                 </div>

              </div>
          </div>

          <!-- <div class="col-lg-4 blog_w3l_left"> -->
            <!-- <div class="blog_w3l_left1">
              <div class="blog_w3l-2">
                <h5>KONSULTASI VIA WHATSAPP</h5><br>
                <a href="https://api.whatsapp.com/send?phone=6282312543008&text=Halo%20Admin%20Saya%20Mau%20Tanya">  <img src="{{asset('web/images/button_whatsapp.png')}}" class="responsive" width="80%"/></a> -->
                <!-- <h3 class="mt-2">pellentesque mi non</h3> -->
                <!-- <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p> -->
              <!-- </div>
            </div> -->
            <!-- <div class="blog_w3l_left2">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
            <!-- <div class="blog_w3l_left3">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
          <!-- </div> -->
          </div>
        </div>
     </section>

     <section class="services py-5" id="services">
      <div class="container py-lg-5">
        <div class="row blog_w3l_top">
          <div class="col-lg-12 blog_w3l_right">

            <h2 class="heading text-center mb-5"> <strong>ALUZE adalah Minuman yang Mengandung Glutation dan Kolagen</strong></h2>
            <div class="col-md-6">
              <center><img src="{{asset('web/images/aluze/aluze-produk2.jpg')}}" width="100%" alt="news image" class="img-fluid"></center>
            </div>
            <div class="col-md-6">
              <center><img src="{{asset('web/images/aluze/aluze-produk1.jpg')}}" width="100%" alt="news image" class="img-fluid"></center>
            </div>
            <div class="col-md-6">
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>Kandungan Serta Manfaat Lain Dari ALUZE.  <br>Komposisi ALUZE : </h6>
                   <p style="text-align:justify;font-size:20px;">- Frutosa (mengandung pengawet sulfit)</p>
                   <p style="text-align:justify;font-size:20px;">- Maltodekstrin</p>
                   <p style="text-align:justify;font-size:20px;">- Kolagen Ikan</p>
                   <p style="text-align:justify;font-size:20px;">- Perisa Sintetik Strawberry</p>
                   <p style="text-align:justify;font-size:20px;">- Vitamin C</p>
                   <p style="text-align:justify;font-size:20px;">- Pengatur Keasaman Asam Sitrat</p>
                   <p style="text-align:justify;font-size:20px;">- Glutation</p>
                   <p style="text-align:justify;font-size:20px;">- Vitamin A (mengandung anti oksidan Tokoferol)</p>
                   <p style="text-align:justify;font-size:20px;">- Pewarna Makanan Karmoisin Cl 14720</p>
                 </div>
              </div>
            </div>
            <div class="col-md-6" style="margin-bottom:50px">
              <div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>Berbagai Macam Manfaat ALUZE :</h6>

                   <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
                       <li>Membantu meningkatkan Glutathione (mother of antioxidant)</li>
                       <li>Melindungi tubuh dari serangan radikal bebas</li>
                       <li>Memperlambat penuaan dini</li>
                       <li>Membantu mencerahkan kulit</li>
                       <li>Membantu mengurangi kekeringan kulit</li>
                       <li>Membantu menghaluskan dan mengurangi kerut serta garis penuaan</li>
                       <li>Meningkatkan kekenyalan dan elastisitas kulit</li>
                       <li>Membantu mengurangi noda hitam</li>
                       <li>Membantu mencegah penyakit degeneratif seperti kencing manis, darah tinggi, penyakit jantung, alzhaimer, Parkinson Dll</li>
                       <li>Mengoptimalkan fungsi sel – sel tubuh</li>
                       <li>Membantu mengoptimalkan system imunitas</li>
                   </ol>

                 </div>
              </div>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-1.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-2.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-3.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-4.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-5.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-6.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-7.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-3" style="margin-bottom:30px;">
              <img src="{{asset('web/images/aluze/ALUZE-fungsi-8.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
            </div>
            <div class="col-md-12">
              <br>
              <center><h2>ALUZE DAPAT MENCEGAH PENUAAN DINI DENGAN CARA PRAKTIS DAN ALAMI !</h2></center>
              <center> <a href=""><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;"  alt="news image" class="img-fluid"/></a> </center><br>
            </div>
          </div>

          <!-- <div class="col-lg-4 blog_w3l_left"> -->
            <!-- <div class="blog_w3l_left1">
              <div class="blog_w3l-2">
                <h5>KONSULTASI VIA WHATSAPP</h5><br>
                <a href="https://api.whatsapp.com/send?phone=6282312543008&text=Halo%20Admin%20Saya%20Mau%20Tanya">  <img src="{{asset('web/images/button_whatsapp.png')}}" class="responsive" width="80%"/></a> -->
                <!-- <h3 class="mt-2">pellentesque mi non</h3> -->
                <!-- <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p> -->
              <!-- </div>
            </div> -->
            <!-- <div class="blog_w3l_left2">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
            <!-- <div class="blog_w3l_left3">
              <div class="blog_w3l-2">
                <h5>20 December 2018</h5>
                <h3 class="mt-2">pellentesque mi non</h3>
                <p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
              </div>
            </div> -->
          <!-- </div> -->
          </div>
        </div>
     </section>
     <section class="contact py-5">
       <div class="container py-lg-5">
     		<div class="row blog_w3l_top">
     			<div class="col-lg-12 blog_w3l_right">
     <h3 class="heading text-center"> <strong> TESTIMONI CHAT</strong> </h3>
     <div class="swiper-container">
       <div class="swiper-wrapper">
           <div class="swiper-slide"><img src="{{asset('web/images/aluze/testimoni-aluze1.jpg')}}" /></div>
           <div class="swiper-slide"><img src="{{asset('web/images/aluze/testimoni-aluze2.jpg')}}" /></div>
           <div class="swiper-slide"><img src="{{asset('web/images/aluze/testimoni-aluze3.jpg')}}" /></div>
           <div class="swiper-slide"><img src="{{asset('web/images/aluze/testimoni-aluze4.jpg')}}" /></div>
           <div class="swiper-slide"><img src="{{asset('web/images/aluze/testimoni-aluze5.jpg')}}" /></div>
       </div>
       <!-- Add Pagination -->
       <div class="swiper-pagination"></div>
     </div>


     </div>
     </div>
     </div>
     </section>


     <!-- <section class="news py-5"> -->
       <div class="container-fluid pb-video-container">
         <div class="col-md-8 col-md-offset-2">
             <h3 class="text-center">TESTIMONI VIDEO</h3>
             <div class="row pb-row">
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/zI94gZQz_V8" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 1</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/kYxXURAsAf0" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 2</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/CbCcKoLy6rQ" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 3</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/OzPmHRzJ_UI" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 4</label>
                 </div>
             </div>
             <div class="row pb-row">
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/PBCgdAxkHlY" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 5</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/ly1s83zoVpk" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 6</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/lJQfZbnVZ5k" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 7</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/9uMMPYF_PXQ" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 8</label>
                 </div>
             </div>
             <div class="row pb-row">
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/k8D9QQcR2J4" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 9</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/9ZXvBQDr8TY" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 10</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/dL9hOG_kfvs" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 11</label>
                 </div>
                 <div class="col-md-3 pb-video">
                     <iframe width="100%" height="230" src="https://www.youtube.com/embed/SZDm7vpyh08" frameborder="0" allowfullscreen></iframe>
                     <label class="form-control label-warning text-center">Testimoni Produk Aluze 12</label>
                 </div>
             </div>
         </div>
     </div>
<!-- <section> -->


<section class="blog_w3l py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-6 blog_w3l_right col-md-offset-3">
  <center><h2>MAU DAPAT DISKON RESELLER ?</h2></center>
            <!-- <center> <a href="{{url('diskon-reseller')}}"> <button type="button" name="button" style=" height:100px;width:300px;background:rgba(240, 183, 24, 1);font-size:40px;font-weight: bold;">MAU !</button> </a></center><br> -->
            <center> <a href="{{url('aluze/diskon-reseller')}}?ref={{$ids}}"><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>

<hr>
  <center><h2>KONSULTASI VIA WA</h2></center>
            <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konsultasi%20Tentang%20Aluze"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
</div></div></div>
</section>







	<!-- //blog -->
	<!-- contact -->
  <section class="contact py-5" id="contact">
		<div class="container py-lg-5">
			<div class="text-center">
				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
			</div>
			<div class="row contact-top">
				<div class="col-lg-12 contact_grid_right">
					<form action="#" method="post">
						<div class="row contact_top">
							<div class="col-sm-12">
								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
							</div>
							<div class="col-sm-12">
								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
							</div>
              <div class="col-sm-12">
								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
							</div>
              <br><br><br>
              <div class="col-sm-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Kirim Data</button>
    							<button type="reset" class="btn">Bersihkan Form</button>
              </div>
						</div>
							<div class="clearfix"> </div>
					</form>
				</div>
		</div>
	</section>
	 <!-- //contact -->
	 <div class="cpy-right text-center">
		<div class="container">
				<p class="py-md-5 py-4">© COPYRIGHT2019
				</p>
			</div>
	</div>
  <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
  <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js'></script>
  <script >var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
  spaceBetween: 0,
    //loop: true,
  autoplay: 2500,
    autoplayDisableOnInteraction: true,
    slidesPerView: 4,
    coverflow: {
        rotate: 30,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    }
  });

  //# sourceURL=pen.js
  </script>
   </body>
</html>
