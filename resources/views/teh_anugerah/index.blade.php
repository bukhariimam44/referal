<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->


      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.min.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!-- <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all"> -->
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">

    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/abennington/pen/GZeyKr?depth=everything&order=popularity&page=45&q=pack&show_forks=false" />

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'><link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
  <style class="cp-pen-styles">img { max-height: 100% }
  .swiper-container {
    width: 100%;
    height: 400px;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  /*width:90%;*/
  }
  div{
    margin-bottom: 20;
  }
  /* carousel */
  .media-carousel
  {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
    margin-top: 30px;
  }
  /* Previous button  */
  .media-carousel .carousel-control.left
  {
    left: -12px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Next button  */
  .media-carousel .carousel-control.right
  {
    right: -12px !important;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Changes the position of the indicators */
  .media-carousel .carousel-indicators
  {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
  }
  /* Changes the colour of the indicators */
  .media-carousel .carousel-indicators
  {
    background: #c0c0c0;
  }
  .media-carousel .carousel-indicators .active
  {
    background: #333333;
  }
  .media-carousel img
  {
    width: 250px;
    height: 100px
  }
  .parav .li-li li{
     text-align:justify;
     font-size:20px; color:#888;
     font-family: 'Source Sans Pro', sans-serif;"
  }
  /* End carousel */

</style>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: 'Source Sans Pro', sans-serif;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #2c3e50;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }

        .pb-video-frame:hover {
            height: 300px;
        }

    .pb-row {
        margin-bottom: 10px;
    }

    .parav .li-li{
       text-align:justify;
       font-size:20px; color:#888;
       font-family: 'Source Sans Pro', sans-serif;"
    }
</style>
</head>
   <body>

	<section class="news py-5" id="about">
		<div class="container py-lg-5">
			<h2 class="heading text-center" style="font-size:30px;">  <strong>MENGABAIKAN OBESITAS (KEGEMUKAN) DAPAT BERAKIBAT FATAL
WASPADA BAHAYA SEBELUM TERLAMBAT
</strong> </h2>

      	<div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/teh_anugerah/lp-teh-1.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>APA ITU OBESITAS (KEGEMUKAN) ?</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Obesitas adalah kondisi kronis akibat penumpukan lemak dalam tubuh yang sangat tinggi. Obesitas terjadi karena asupan kalori yang lebih banyak dibanding aktivitas membakar kalori, sehingga kalori yang berlebih menumpuk dalam bentuk lemak. Kondisi tersebut dalam waktu lama menambah berat badan hingga orang tersebut mengalami obesitas.</p>
            <p style="text-align:justify;font-size:20px;">Penumpukan lemak tubuh ini meningkatkan risiko gangguan kesehatan serius, seperti penyakit jantung, diabetes, atau hipertensi. Obesitas juga dapat menyebabkan gangguan kualitas hidup dan masalah psikologi, seperti kurang percaya diri hingga depresi.</p>
            <p style="text-align:justify;font-size:20px;">Masalah obesitas ini terkait dengan peningkatan jumlah kematian akibat penyakit jantung dan pembuluh darah, diabetes, serta beberapa penyakit kanker. Jumlah kematian penderita obesitas yang disertai sejumlah penyakit tersebut lebih banyak dibanding penderita dengan berat badan yang normal.</p>

                </div>
		</div>
	</section>

<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/teh_anugerah/lp-teh-2.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>PENYEBAB OBESITAS</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Obesitas dapat terjadi ketika kita sering mengonsumsi makanan dan minuman tinggi kalori, dengan tidak diimbangi dengan aktivitas fisik yang sesuai. Kebutuhan rata-rata kalori bagi wanita dewasa yang aktif secara fisik per hari adalah sekitar 2000 kalori, sedangkan bagi pria dewasa yang juga aktif secara fisik adalah 2500 kalori.</p>
            <p style="text-align:justify;font-size:20px;">Selain pola makan yang tidak sehat serta tubuh yang kurang aktif bergerak, obesitas juga bisa disebabkan oleh faktor-faktor lainnya, seperti:</p>
<ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
  <li> <strong> Keturunan atau genetik.</strong> Faktor ini dapat berpengaruh pada jumlah lemak yang diserap tubuh atau digunakan sebagai energi.</li>
  <li> <strong> Efek samping obat-obatan.</strong> Beberapa jenis obat yang dapat menyebabkan kenaikan berat badan adalah antidepresan, antipsikotik, antikonvulsan, kortikosteroid, obat diabetes, dan obat penghambat beta.</li>
  <li> <strong> Keturunan atau genetik.</strong> Saat hamil, wanita akan membutuhkan banyak asupan nutrisi dari makanan. Namun tidak sedikit pula dari mereka yang mengalami kesulitan untuk menurunkan berat badannya kembali setelah melahirkan.</li>
  <li> <strong> Kurang tidur.</strong> Perubahan hormon yang terjadi ketika kita kurang tidur dapat meningkatkan nafsu makan. Hal ini dapat mengarah kepada obesitas.</li>
  <li> <strong> Pertambahan usia.</strong> Makin tua usia kita, maka makin besar pula risiko bertambahnya berat badan. Hal ini diakibatkan oleh metabolisme tubuh yang menurun dan massa otot yang berkurang.</li>

</ol>
                </div>
			</div>
			</div>
		</div>
</section>

<section class="news py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/teh_anugerah/lp-teh-3.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>DAMPAK BURUK DARI OBESITAS (KEGEMUKAN)</strong></center> </h2>

          <div class="blog_w3l-5">
            <ol type="a" style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
              <li> <strong>Penyakit kronis</strong> <br> Munculnya penyakit kronis merupakan bahaya kegemukan yang sangat perlu diwaspadai. Penyakit-penyakit yang paling sering dikaitkan dengan bahaya kegemukan, antara lain penyakit jantung, tekanan darah tinggi, kadar kolesterol tinggi, penyakit hati dan kandung empedu. Kelebihan berat badan juga dapat memengaruhi kinerja hormon insulin penyebab diabetes, yang akan meningkatkan risiko penyakit jantung dan stroke. Lemak berlebih mengganggu jantung karena menimbulkan timbunan kolesterol, kenaikan tekanan darah, dan penyumbatan arteri. Pada wanita, bahaya kegemukan dapat mengakibatkan datang bulan yang tidak teratur dan infertilitas.</li>
              <li> <strong>Menyebabkan gangguan tidur</strong> <br> Orang yang terlalu gemuk lebih berisiko mengalami gangguan tidur. Salah satunya adalah sleep apnea, yaitu kondisi saat orang berhenti bernapas beberapa kali saat tidur. Ini merupakan salah satu gangguan tidur yang serius. Kondisi ini membuat kadar oksigen turun drastis, sehingga memengaruhi jantung dan pembuluh darah serta meningkatkan risiko diabetes dan stroke.</li>
              <li> <strong>Menyebabkan gangguan pada lutut dan pinggang</strong> <br> Kelebihan berat badan dapat meningkatkan risiko osteoporosis. Di samping itu, Anda juga berisiko mengalami nyeri pada sendi lutut dan punggung akibat tubuh menopang beban yang berlebih.</li>
              <li> <strong>Meningkatkan risiko terkena kanker</strong> <br> Pada pria, kegemukan juga dapat mengakibatkan meningkatnya risiko terkena berbagai jenis kanker, seperti kanker esofagus, kanker ginjal, kanker pankreas, kanker tiroid, dan kanker kolon. Sementara pada wanita, kegemukan dapat meningkatkan risiko terkena kanker ginjal, kanker kandung empedu, kanker esofagus, dan kanker rahim.</li>
              <li> <strong>Diskriminasi dan tidak percaya diri</strong> <br> Remaja yang memiliki tubuh gemuk cenderung memiliki masalah psikologis dan sosial. Mereka dapat tumbuh menjadi remaja yang tidak percaya diri dan mengalami diskriminasi atau perundungan (bullying) dari teman-teman sekitarnya.</li>

            </ol>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="ban_bottom1 py-5" id="more">
	<div class="container py-lg-5">
		<div class="ban_bottom_top text-center py-lg-5">
			<h3>LALU BAGAIMANA SOLUSI MENGATASI OBESITAS ?</h3>
		</div>
	</div>
</section>
<section class="services py-5" id="blog">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/teh_anugerah/lp-teh-5.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>TEH HITAM</strong></center> </h2>

          <div class="blog_w3l-5">
            <p style="text-align:justify;font-size:20px;">Banyak penelitian telah mengungkap bahwa teh hitam ternyata bermanfaat mencegah obesitas dan meningkatkan kesehatan tubuh. Penelitian menunjukkan bahwa kandungan kimia yang terdapat dalam teh hitam, yang disebut polifenol, dapat mengubah proses metabolisme energi di hati (liver) dengan cara mengubah metabolit bakteri di usus. Selain itu, masih banyak manfaat teh hitam untuk menurunkan berat badan. Simak penjelasan lengkapnya di bawah ini, yuk!</p>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="news py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Manfaat polifenol dalam teh hitam untuk menurunkan berat badan</strong></center> </h2>

          <div class="blog_w3l-5">
            <p style="text-align:justify;font-size:20px;">Sekelompok antioksidan berbasis tumbuhan, yang disebut flavonoid atau polifenol, berperan atas banyak manfaat kesehatan yang berasal dari teh hitam yang mengandung sekelompok flavonoid yang disebut katekin. Saat daun teh diproses lebih lanjut untuk menghasilkan teh hitam, katekin membentuk flavonoid baru yang disebut theaflavin dan thearubigin. Teh hitam masih mengandung sejumlah kecil katekin, tapi manfaat kesehatan yang terbesar berasal dari flavonoid baru.</p>
            <p style="text-align:justify;font-size:20px;">Penelitian sejauh ini menunjukkan bahwa teh hitam memiliki potensi untuk membantu menurunkan berat badan. Enzim pencernaan lipase dapat dihambat pada hewan percobaan yang mengonsumsi flavonoid teh hitam. Karena lemak tidak dapat dicerna tanpa enzim lipase, beberapa lemak tidak diserap oleh tubuh, melainkan dikeluarkan dari tubuh.</p>
            <p style="text-align:justify;font-size:20px;">Ketika tikus percobaan diberi makan makanan tinggi lemak, tikus yang menerima dosis polifenol teh hitam lebih tinggi kehilangan berat badan lebih banyak daripada kelompok yang mendapat lebih sedikit polifenol. Peneliti juga melaporkan bahwa terdapat peningkatan pembakaran energi atau kalori yang cukup berarti setelah tikus percobaan mendapatkan theaflavin dari teh hitam.</p>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/teh_anugerah/lp-teh-6.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
          <div class="blog_w3l-5">
            <h6>Manfaat kafein dalam teh hitam untuk meningkatkan metabolisme</h6>
						<p style="text-align:justify;font-size:20px;">Ketika Anda minum secangkir teh hitam biasa, Anda akan mendapatkan 30 sampai 80 miligram kafein. Hanya dibutuhkan sekitar 50 miligram kafein untuk meningkatkan jumlah energi yang dibakar tubuh Anda saat beristirahat, menurut sebuah studi dalam European Journal of Clinical Nutrition.</p>
            <p style="text-align:justify;font-size:20px;">Penelitian mengungkapkan bahwa kafein dapat meningkatkan metabolisme basal tubuh sebesar 6 persen. Kafein juga mendorong lipolisis, yaitu proses pemecahan lemak tubuh serta merangsang siklus tubuh yang memetabolisme lemak.</p>
            <p style="text-align:justify;font-size:20px;">Kafein bisa membantu mempertahankan penurunan berat badan untuk jangka waktu yang lebih lama. Dari 2.000 orang yang disurvei, sebanyak hampir 500 orang melaporkan bahwa mereka berhasil menurunkan berat badan dan mempertahankannya.</p>
            <br>
            <h6>Manfaat teh hitam untuk mengurangi asupan kalori</h6>
            <p style="text-align:justify;font-size:20px;">Selain manfaat potensial dari polifenol dan kafein, teh hitam akan membantu menurunkan berat badan jika Anda meminumnya sebagai pengganti minuman berkalori tinggi seperti soda atau minuman kemasan. Secangkir teh hitam hanya mengandung dua kalori.</p>
            <p style="text-align:justify;font-size:20px;">Bahkan jika Anda menambahkan satu sendok madu, teh hitam hanya mengandung 23 kalori. Jika Anda memiliki kebiasaan minum minuman manis, gantilah dengan teh hitam tanpa gula sehingga dapat mengurangi sejumlah besar asupan kalori harian Anda.</p>
            <p style="text-align:justify;font-size:20px;">Minum sebelum makan bisa membuat Anda merasa lebih kenyang, yang lantas membantu sebagian orang untuk makan dalam jumlah yang lebih sedikit. Dua penelitian tentag obesitas pada tahun 2010 dan 2015 menemukan bahwa orang yang minum sebelum makan berhasil menurunkan berat badan lebih banyak dibandingkan dengan orang yang tidak minum apapun sebelum makan.</p>
            <p style="text-align:justify;font-size:20px;">Anda juga akan membakar beberapa kalori tambahan karena tubuh memetabolisme air. Meskipun penelitian ini hanya menggunakan air biasa, manfaat teh hitam bagi penurunan berat badan dan pengendalian asupan kalori juga serupa.</p>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="blog py-5" id="about">
  <div class="container py-lg-5">
    <h2 class="heading text-center" style="font-size:30px;">  <strong>TEH ANUGERAH ADALAH TEH HITAM BERKUALITAS TINGGI</strong> </h2>

      <div class="row news-grids py-lg-5 mt-3 text-center">
          <div class="col-md-12 newsgrid1">
            <img src="{{asset('web/images/teh_anugerah/lp-teh-7.png')}}" alt="news image" class="img-fluid">
          </div>

      </div>
      <!-- <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>APA ITU OBESITAS (KEGEMUKAN) ?</strong></center> </h2> -->

        <div class="blog_w3l-5">
          <h6>Kandungan serta manfaat lain dari TEH ANUGERAH</h6>
          <h6>Komposisi TEH ANUGERAH	:</h6>
          <li style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">3 Daun Teh Pucuk pilihan</li>
          <h6>Manfaat TEH ANUGERAH</h6>
          <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
            <li>Sebagai obat menurunkan berat badan dan obesitas</li>
            <li>Mengandung antioksidan tinggi untuk mencegah kerusakan yang disebabkan oleh radikal bebas sehingga memicu berbagai macam penyakit</li>
            <li>Membantu mengurangi resiko serangan jantung dan stroke</li>
            <li>Membantu melawan kanker dengan memperlambat pertumbuhan sel kanker</li>
            <li>Membantu menjaga kelembaban dan kecerahan kulit wajah (anti aging alami)</li>
            <li>Membantu mengurangi kolesterol jahat dan meningkatkan kolesterol baik</li>
            <li>Membantu menormalkan kadar asam urat didalam tubuh</li>
            <li>Membantu pencegahan gejala flu, batuk, demam tinggi dan sakit kepala/migrain</li>
            <li>Membantu menurunkan kadar glukosa darah sehingga bermanfaat membantu penyembuhan penyakit dibetes</li>
            <li>Membantu meningkatkan vitalitas dan stamin</li>
          </ol>
              </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-1.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-2.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-3.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-4.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-5.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-6.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-7.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>
                <div class="col-md-3" style="margin-bottom: 20px;">
                  <img src="{{asset('web/images/teh_anugerah/teh-anugerah-fungsi-8.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
                </div>

              <div class="col-md-12">
                <center><h2>MAU MENURUNKAN BERAT BADAN DAN OBESITAS SECARA PRAKTIS DAN ALAMI ?</h2></center>
                <center> <a href=""><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>
              </div>
  </div>
</section>

<section class="contact py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
<h3 class="heading text-center"> <strong> TESTIMONI CHAT</strong> </h3>
<div class="swiper-container">
  <div class="swiper-wrapper">
      <div class="swiper-slide"><img src="{{asset('web/images/teh_anugerah/chat-teh-1.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/teh_anugerah/chat-teh-2.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/teh_anugerah/chat-teh-3.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/teh_anugerah/chat-teh-4.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/teh_anugerah/chat-teh-5.jpg')}}" /></div>
  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>


</div>
</div>
</div>
</section>


<!-- <section class="news py-5"> -->
  <div class="container-fluid pb-video-container">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="text-center">TESTIMONI VIDEO</h3>
        <div class="row pb-row">
            <div class="col-md-4 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/ulxnv8x_fXM" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Teh Anugerah 1</label>
            </div>
            <div class="col-md-4 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/Znzqf1vbyd4" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Teh Anugerah 2</label>
            </div>
            <div class="col-md-4 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/ytjmA3HVu6s" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Teh Anugerah 3</label>
            </div>
        </div>
    </div>
</div>


<!-- <section> -->
<section class="blog_w3l py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-6 blog_w3l_right col-md-offset-3">
  <center><h2>MAU DAPAT DISKON RESELLER ?</h2></center>
  <center> <a href="{{url('teh-anugerah/diskon-reseller')}}?ref={{$ids}}"><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>

<hr>
  <center><h2>KONSULTASI VIA WA</h2></center>
            <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konsultasi%20Tentang%20Teh%20Anugerah"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
</div></div></div>
</section>







	<!-- //blog -->
	<!-- contact -->
	<section class="contact py-5" id="contact">
		<div class="container py-lg-5">
			<div class="text-center">
				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
			</div>
			<div class="row contact-top">
				<div class="col-lg-12 contact_grid_right">
					<form action="#" method="post">
						<div class="row contact_top">
							<div class="col-sm-12">
								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
							</div>
							<div class="col-sm-12">
								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
							</div>
              <div class="col-sm-12">
								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
							</div>
              <br><br><br>
              <div class="col-sm-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Kirim Data</button>
    							<button type="reset" class="btn">Bersihkan Form</button>
              </div>
						</div>
							<div class="clearfix"> </div>
					</form>
				</div>
		</div>
	</section>
	 <!-- //contact -->
	 <div class="cpy-right text-center">
		<div class="container">
				<p class="py-md-5 py-4">© COPYRIGHT2019
				</p>
			</div>
	</div>
  <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
  <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js'></script>
  <script >var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
  spaceBetween: 0,
    //loop: true,
  autoplay: 2500,
    autoplayDisableOnInteraction: true,
    slidesPerView: 4,
    coverflow: {
        rotate: 30,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    }
  });

  //# sourceURL=pen.js
  </script>
   </body>
</html>
