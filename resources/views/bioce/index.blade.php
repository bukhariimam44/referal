<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->


      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.min.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!-- <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all"> -->
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">

    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/abennington/pen/GZeyKr?depth=everything&order=popularity&page=45&q=pack&show_forks=false" />

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'><link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
  <style class="cp-pen-styles">img { max-height: 100% }
  .swiper-container {
    width: 100%;
    height: 400px;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  /*width:90%;*/
  }
  div{
    margin-bottom: 20;
  }
  /* carousel */
  .media-carousel
  {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
    margin-top: 30px;
  }
  /* Previous button  */
  .media-carousel .carousel-control.left
  {
    left: -12px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Next button  */
  .media-carousel .carousel-control.right
  {
    right: -12px !important;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Changes the position of the indicators */
  .media-carousel .carousel-indicators
  {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
  }
  /* Changes the colour of the indicators */
  .media-carousel .carousel-indicators
  {
    background: #c0c0c0;
  }
  .media-carousel .carousel-indicators .active
  {
    background: #333333;
  }
  .media-carousel img
  {
    width: 250px;
    height: 100px
  }
  .parav .li-li li{
     text-align:justify;
     font-size:20px; color:#888;
     font-family: 'Source Sans Pro', sans-serif;"
  }
  /* End carousel */

</style>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: 'Source Sans Pro', sans-serif;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #2c3e50;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }

        .pb-video-frame:hover {
            height: 300px;
        }

    .pb-row {
        margin-bottom: 10px;
    }

    .parav .li-li{
       text-align:justify;
       font-size:20px; color:#888;
       font-family: 'Source Sans Pro', sans-serif;"
    }
</style>
</head>
   <body>

	<section class="news py-5" id="about">
		<div class="container py-lg-5">
			<h2 class="heading text-center" style="font-size:30px;">  <strong>RADANG TENGGOROKAN JANGAN DIANGGAP REMEH</strong> </h2>
      <p style="text-align:justify;font-size:20px;">Radang tenggorokan akut ditandai dengan gejala nyeri pada tenggorokan, nyeri saat menelan, demam, sakit kepala, badan lemas, kadang disertai batuk dan pilek, sedangkan radang tenggorok kronik bisa berefek pembengkakan tonsil (amandel) hingga pembengkakan kelenjar getah bening.</p>
				<div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/bioce/bioce1.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
		</div>
	</section>

<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Apa itu radang tenggorokan (faringitis)?</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Radang tenggorokan atau faringis adalah sakit tenggorokan yang disebabkan oleh radang bagian belakang tenggorokan, atau yang dalam dunia medis disebut dengan faring. Masyarakat Indonesia sering menyebutnya dengan sebutan panas dalam. Radang tenggorokan akan membuat Anda merasa tidak nyaman karena tenggorokan akan terasa sakit atau panas, sehingga membuat Anda kesulitan untuk makan.</p>
            <p style="text-align:justify;font-size:20px;">Sakit tenggorokan adalah gejala umum dari beberapa penyakit yang berbeda atau terjadi karena penyakit lain, seperti flu, demam dan mononukleosis. Sakit tenggorokan biasanya akan mereda tanpa obat radang tenggorokan dalam waktu kurang dari seminggu.</p>

            </div>
			</div>
			</div>
		</div>
</section>
<section class="news py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Seberapa umumkah radang tenggorokan (faringitis)</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Kondisi ini dapat memengaruhi semua orang, tanpa memandang usia dan jenis kelamin. Siapapun bisa mengalami radang tenggorokan, baik anak-anak, orang dewasa, maupun orang lanjut usia. Namun, anak-anak berusia antara 5 sampai 15 tahun cenderung paling sering terkena sakit tenggorokan.
Pada orang dewasa, 10% sakit tenggorokan yang mereka alami disebabkan oleh infeksi bakteri streptococcus. Anda dapat mengatasi radang tenggorokan ini dengan mengurangi faktor risiko. Hati-hati!</p>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="services py-5" id="services">
	<div class="container py-lg-5">
    <h2 class="heading text-center" style="font-size:30px;">  <strong>Radang tenggorokan mudah menular</strong> </h2>
		<div class="col-lg-12 blog_w3l_right">
      <p style="text-align:justify;font-size:20px;">Bakteri yang menyebabkan radang tenggorokan sangat mudah menular. Penderita dapat menyebarkannya melalui kontak dekat, termasuk bersin dan jabat tangan, atau saat menggunakan barang pribadi bergantian dengan orang lain.</p>
      <center> <img src="{{asset('web/images/bioce/bioce2.png')}}" alt="news image" class="img-fluid"></center><br>
      <h6>APA SAJA CIRI-CIRI DAN GEJALA RADANG TENGGOROKAN (FARINGITIS)?</h6>
      <p style="text-align:justify;font-size:20px;">Jika Anda mengalami radang tenggorokan, biasanya Anda akan mengalami rasa tidak nyaman dalam tenggorokan. Gejala lain yang muncul biasanya tergantung pada penyebabnya. Gejala umum yang muncul pada radang tenggorokan adalah:</p>
      <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
        <li>Sakit tenggorokan</li>
        <li>Demam</li>
        <li>Sakit Kepala</li>
        <li>Nyeri sendi dan nyeri otot</li>
        <li>Ruam Kulit</li>
        <li>Membengkaknya kelenjar getah bening di leher</li>

      </ol>
      <p style="text-align:justify;font-size:20px;">Karena gejala radang tenggorokan bergantung pada penyebabnya, maka gejala sakit tenggorokan yang Anda rasakan bisa bervariasi. Gejala sakit tenggorokan yang disebabkan oleh demam adalah:</p>
      <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
        <li>Bersin</li>
        <li>Batuk</li>
        <li>Demam dengan suhu 38 derajat Celsius</li>
        <li>Sakit kepala ringan</li>
      </ol>
      <p style="text-align:justify;font-size:20px;">Sedangkan gejala sakit tenggorokan yang disebabkan oleh flu adalah:</p>
      <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
        <li>Kelelahan</li>
        <li>Pegal-pegal</li>
        <li>Panas dingin</li>
        <li>Demam dengan suhu lebih dari 38 derajat Celsius</li>
      </ol>
      <p style="text-align:justify;font-size:20px;">Sedangkan gejala sakit tenggorokan yang disebabkan oleh mononukleosis atau demam kelenjar adalah:</p>
      <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
        <li>Pembesaran kelenjar getah bening di leher dan ketiak</li>
        <li>Amandel membengkak</li>
        <li>Sakit kepala</li>
        <li>Kehilangan selera makan</li>
        <li>Pembengkakan limpa</li>
        <li>Peradangan hati</li>
      </ol>
    </div>

	</div>
</section>
<section class="ban_bottom1 py-5" id="more">
	<div class="container py-lg-5">
		<div class="ban_bottom_top text-center py-lg-5">
			<h3>Sekarang  ada solusi praktis & alami tanpa bahan-bahan kimia yang bisa membantu anda mengatasi masalah Flu dan Radang Tenggorokan, yaitu:</h3>
		</div>
	</div>
</section>
<section class="blog_w3l py-5" id="services">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top" style="line-height: 30px;">
			<div class="col-lg-12 blog_w3l_right">
        <h2 class="heading text-center" style="font-size:30px;">  <strong>BIOCE SILVER SPRAY</strong> </h2>
					<center><img src="{{asset('web/images/bioce/bioce4.png')}}" alt="news image" class="img-fluid"></center><br><br>
          <!-- <h4 class="heading text-left mb-5"><center><strong>BIOCE SILVER SPRAY</strong></center></h4> -->
          <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;"><strong>BIOCE SILVER SPRAY</strong> adalah air ionic (air perak murni). Ion Perak Ag+ atau Ion Silver adalah air tanpa bau, tawar, tidak pedas, tidak berbahaya untuk mata, tidak ada radikal bebas, tidak berbahaya untuk manusia dan tidak memiliki reaksi dengan obat lainnya. Ion perak meningkatkan pencernaan, membantu regenerasi sel yang rusak, membantu mencegah demam, flu, dan penyakit yang disebabkan organisme, baik bakteri, jamur, parasit maupun virus.</p>
          <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Penggunaan ion perak tidak membahayakan bakteri baik di dalam pencernaan. Oleh karena itu, ion perak tidak menimbulkan mual atau tumbuhnya jamur yang biasanya terlihat pada penggunaan antibiotik farmasi. Ion Perak Ag+ Membunuh dan Melawan Virus, Bakteri, Parasit, & Jamur hingga 650 jenis dalam waktu 6 Menit! Antibiotik hanya mampu membunuh 7 jenis saja.</p>
					<div class="blog_w3l-5">
              <h6>Khasiat/Manfaat/Fungsi Utama Ion Perak Ag+ :</h6>
              <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
                <li>Sangat cocok terutama untuk pengobatan/terapi luar; untuk pengobatan/terapi dalam, gunakan dosis kecil.</li>
                <li>Untuk menghancurkan infeksi dan penyembuhan diri.</li>
                <li>Untuk mengaktifkan sel yang rusak dan menjadikan sel meregenerasi sel baru yang bagus.</li>
                <li>Tidak mengandung radikal bebas, aman untuk enzim tubuh manusia dan tidak ada efek samping yang negatif jika digunakan bersamaan dengan obat, jamu, atau suplemen lainnya.</li>
                <li>Membantu memperbaiki pencernaan, regenerasi sel dan jaringan yang rusak, mencegah pilek dan flu, alergi dan berbagai penyakit yang disebabkan berbagai jenis bakteri, kuman dan infeksi virus.</li>
                <li>Non-Toxic dan tidak berbahaya terhadap ginjal, hati, mata, dan organ tubuh lainnya.</li>
                <li>Bila digunakan setiap hari secara teratur, dapat membantu meningkatkan sistem kekebalan tubuh.</li>
              </ol>

            </div>
<br>
					</div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide1.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide2.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide3.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide4.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide5.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide6.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide7.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide8.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide9.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-2" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/bioce/Slide10.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-12">
            <center><h2>MAU GEJALA FLU DAN RADANG TENGGOROKAN HILANG
DENGAN CARA PRAKTIS & ALAMI</h2></center>
            <center> <a href=""><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>
          </div>
			</div>
		</div>
</section>

<section class="contact py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
<h3 class="heading text-center"> <strong> TESTIMONI CHAT</strong> </h3>
<div class="swiper-container">
  <div class="swiper-wrapper">
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-1.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-2.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-3.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-4.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-5.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-6.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-7.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-8.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-9.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-10.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-11.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-12.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-13.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/bioce/testimoni-bioce-14.jpg')}}" /></div>
  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>


</div>
</div>
</div>
</section>


<!-- <section class="news py-5"> -->
  <div class="container-fluid pb-video-container">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="text-center">TESTIMONI VIDEO</h3>
        <div class="row pb-row">
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/drhQ4aY_Jq8" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 1</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/DC1O7clV7Dk" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 2</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/4z628tbCTqI" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 3</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/Mgh3WnSDMtA" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 4</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/BVAbM0uduFs" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 5</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/sWmQO2K7uIg" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 6</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/_4RrmKz0dG0" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 7</label>
            </div>
            <div class="col-md-3 pb-video">

                <iframe width="100%" height="230" src="https://www.youtube.com/embed/AFHs0unDpKc" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Bioce 8</label>
            </div>

        </div>
        <!-- <div class="row pb-row">
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/UY1bt8ilps4?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">F.O. and Peeva - Lichnata</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/QpbQ4I3Eidg?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Machine Gun - Bad Things</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/h3kRIxLruDs?ecver=" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">INNA - Say it with your body</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/Jr4TMIU9oQ4?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">INNA - Gimme Gimme</label>
            </div>
        </div> -->
    </div>
</div>


<!-- <section> -->
<section class="blog_w3l py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-6 blog_w3l_right col-md-offset-3">
  <center><h2>MAU DAPAT DISKON RESELLER ?</h2></center>
  <center> <a href="{{url('bioce/diskon-reseller')}}?ref={{$ids}}"><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>

<hr>
  <center><h2>KONSULTASI VIA WA</h2></center>
            <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konsultasi%20Tentang%20Elplus"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
</div></div></div>
</section>







	<!-- //blog -->
	<!-- contact -->
	<section class="contact py-5" id="contact">
		<div class="container py-lg-5">
			<div class="text-center">
				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
			</div>
			<div class="row contact-top">
				<div class="col-lg-12 contact_grid_right">
					<form action="#" method="post">
						<div class="row contact_top">
							<div class="col-sm-12">
								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
							</div>
							<div class="col-sm-12">
								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
							</div>
              <div class="col-sm-12">
								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
							</div>
              <br><br><br>
              <div class="col-sm-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Kirim Data</button>
    							<button type="reset" class="btn">Bersihkan Form</button>
              </div>
						</div>
							<div class="clearfix"> </div>
					</form>
				</div>
		</div>
	</section>
	 <!-- //contact -->
	 <div class="cpy-right text-center">
		<div class="container">
				<p class="py-md-5 py-4">© COPYRIGHT2019
				</p>
			</div>
	</div>
  <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
  <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js'></script>
  <script >var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
  spaceBetween: 0,
    //loop: true,
  autoplay: 2500,
    autoplayDisableOnInteraction: true,
    slidesPerView: 4,
    coverflow: {
        rotate: 30,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    }
  });

  //# sourceURL=pen.js
  </script>
   </body>
</html>
