<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->


      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.min.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!-- <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all"> -->
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">

    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/abennington/pen/GZeyKr?depth=everything&order=popularity&page=45&q=pack&show_forks=false" />

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'><link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
  <style class="cp-pen-styles">img { max-height: 100% }
  .swiper-container {
    width: 100%;
    height: 400px;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  /*width:90%;*/
  }
  div{
    margin-bottom: 20;
  }
  /* carousel */
  .media-carousel
  {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
    margin-top: 30px;
  }
  /* Previous button  */
  .media-carousel .carousel-control.left
  {
    left: -12px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Next button  */
  .media-carousel .carousel-control.right
  {
    right: -12px !important;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Changes the position of the indicators */
  .media-carousel .carousel-indicators
  {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
  }
  /* Changes the colour of the indicators */
  .media-carousel .carousel-indicators li
  {
    background: #c0c0c0;
  }
  .media-carousel .carousel-indicators .active
  {
    background: #333333;
  }
  .media-carousel img
  {
    width: 250px;
    height: 100px
  }
  .parav .li-li li{
     text-align:justify;
     font-size:20px; color:#888;
     font-family: 'Source Sans Pro', sans-serif;"
  }
  /* End carousel */

</style>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: 'Source Sans Pro', sans-serif;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #2c3e50;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }

        .pb-video-frame:hover {
            height: 300px;
        }

    .pb-row {
        margin-bottom: 10px;
    }

    .parav .li-li{
       text-align:justify;
       font-size:20px; color:#888;
       font-family: 'Source Sans Pro', sans-serif;"
    }
</style>
</head>
   <body>

	<section class="news py-5" id="about">
		<div class="container py-lg-5">
			<h2 class="heading text-center" style="font-size:30px;">  <strong>AWAS! BAHAYA DIABETES MELITUS ATAU KENCING MANIS MENGINTAI ANDA</strong> </h2>
				<div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/elplus/elplus1.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
		</div>
	</section>

<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>APAKAH SEBENARNYA DIABETES MELITUS ATAU KENCING MANIS ITU?</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Diabetes adalah suatu penyakit di mana tubuh tidak dapat menghasilkan insulin (hormon pengatur gula darah) atau insulin yang dihasilkan tidak mencukupi atau insulin tidak bekerja dengan baik. Oleh karena itu, penyakit ini akan menyebabkan gula darah meningkat saat diperiksa.</p>

            </div>
			</div>
			</div>
		</div>
</section>
<section class="news py-5" id="about">
  <div class="container py-lg-5">
		<h2 class="heading text-center" style="font-size:30px;"> <strong>BAGAIMANA CARA MENGETAHUI APAKAH KITA MENDERITA DIABETES ATAU TIDAK?</strong> </h3>
    <div class="col-lg-12 blog_w3l_right">
      <h4 class="heading text-left mb-5"> <strong> Mereka yang menderita diabetes sering menunjukkan gejala sebagai berikut:</strong></h4>
        <!-- <img src="{{asset('web/images/penyebab.jpg')}}" alt="news image" class="img-fluid"> -->
        <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
          <li>Haus dan banyak minum</li>
          <li>Lapar dan banyak makan</li>
          <li>Sering kencing</li>
          <li>Berat badan menurun</li>
          <li>Mata kabur</li>
          <li>Luka lama sembuh</li>
          <li>Mudah terjadi infeksi pada kulit (gatal-gatal), saluran kencing dan gusi</li>
          <li>Nyeri atau baal pada tangan atau kaki</li>
          <li>Badan terasa lemah</li>
          <li>Mudah mengantuk</li>
        </ol>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Gejala-gejala di atas sering dijumpai, tapi pada beberapa orang sering tidak dijumpai gejala sama sekali. Untuk memastikannya, diperlukan pemeriksaaan darah di laboratorium.</p>
    </div>
    <div class="col-lg-12 blog_w3l_right"><br>
      <h4 class="heading text-left mb-5"> <strong> Ada 3 jenis tipe dalam diabetes, yaitu:</strong></h4>
          <div class="blog_w3l-8">
          <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
            <li><strong>Diabetes tipe 1</strong></li>
            <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Suatu keadaan di mana tubuh sudah sama sekali tidak dapat memproduksi hormon insulin sehingga penderita harus menggunakan suntikan insulin dalam mengatur gula darahnya. Sebagian besar penderitanya adalah anak-anak & remaja.</p>
            <li><strong>Diabetes tipe 2</strong></li>
            <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Terjadi karena tubuh tidak memproduksi hormon insulin yang mencukupi atau karena insulin tidak dapat digunakan dengan baik (resistensi insulin). Tipe ini merupakan yang terbanyak diderita saat ini (90% lebih), sering terjadi pada mereka yang berusia lebih dari 40 tahun, gemuk, dan mempunyai riwayat diabetes dalam keluarga.</p>
            <li><strong>Diabetes tipe 3</strong></li>
            <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Kondisi sementara dalam kehamilan
90% penderita diabetes di seluruh dunia merupakan pendeita diabetes tipe 2 yang disebabkan oleh gaya hidup yang kurang sehat.</p>

          </ol>
        </div><br>

    </div>
	</div>
</section>
<section class="services py-5" id="services">
	<div class="container py-lg-5">
    <h2 class="heading text-center" style="font-size:30px;">  <strong>AKIBAT PENYAKIT DIABETES</strong> </h2>
		<div class="col-lg-12 blog_w3l_right">
      <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
        <li><strong>Merusak pembuluh darah</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Gula darah tinggi diketahui bisa merusak elastisitas pembuluh darah yang kemudian menyebabkan penyempitan. Beberapa komplikasi yang terjadi karena penyempitan pembuluh darah adalah serangan jantung, stroke, hingga kerusakan organ.</p>
        <li><strong>Merusak saraf</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Ketika sirkulasi darah memburuk selama bertahun-tahun, penderita diabetes juga bisa mengalami kerusakan saraf. Dampak yang paling umum terjadi adalah mati rasa di jari kaki, tangan, hingga kaki yang kemudian terkait dengan peningkatan cedera.</p>
        <li><strong>Dapat menyebabkan gagal ginjal</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Menurut penelitian dari American Diabetes Association, 44% dari kegagalan ginjal terjadi akibat diabetes. Sebabnya, pembuluh darah yang rusak karena diabetes membuat ginjal gagal menyaring limbah secara efisien. Hasilnya terjadi penumpukan racun di ginjal.</p>
        <li><strong>Meningkatkan risiko kebutaan</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Lagi-lagi akibat penyempitan pembuluh darah, risiko kebutaan jadi meninggi. Sebabnya, komplikasi mikrovaskular ini membuat mata tidak mendapatkan suplai darah yang mencukupi.</p>
        <li><strong>Menyebabkan gastroparesis</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Gastroparesis adalah kondisi di mana gerakan alami makanan di saluran pencernaan melambat. Gangguan ini terjadi akibat kerusakan saraf. Beberapa gejala yang menyertainya adalah mual, kembung, naiknya asam lambung, dan nyeri perut.</p>
        <li><strong>Mempengaruhi kehidupan seks</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Komplikasi mikrovaskular dan neurologis diabetes diketahui bisa menyebabkan disfungsi seksual. Pada pria, disfungsi seksual ini akan meningkatkan risiko impotensi, sedangkan pada wanita bisa menyebabkan kekeringan vagina, libido seksual yang turun, dan hubungan seksual yang terasa menyakitkan.</p>
        <li><strong>Luka sulit sembuh</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Sirkulasi darah yang buruk juga bisa membuat luka jadi sulit sembuh atau butuh waktu lama untuk sembuh. Plus, luka yang sulit untuk sembuh sangatlah ideal untuk pertumbuhan mikroba. Itulah sebabnya luka jadi bernanah.</p>
        <li><strong>Mempengaruhi kesehatan kulit</strong></li>
        <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Awal terdiagnosis diabetes, penderita akan mengeluhkan kulit yang kering dan munculnya bercak gelap atau acanthosis nigrans. Setelah itu, penderita akan rentan mengalami bisul hingga jamur.</p>

      </ol>
    </div>

	</div>
</section>
<section class="news py-5" id="services">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <h2 class="heading text-center" style="font-size:30px;">  <strong>JANGAN SAMPAI PENYAKIT DIABETES DIBIARKAN DAN BERAKIBAT BURUK SEPERTI INI</strong> </h2>
					<center><img src="{{asset('web/images/elplus/elplus2.png')}}" alt="news image" class="img-fluid"></center><br><br>
          <h4 class="heading text-left mb-5"><center><strong>LALU BAGAIMANA SOLUSI MENGATASINYA?</strong></center></h4>
          <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Tahukah Anda Ada Solusi Alami Yang Bisa Membantu Anda Mengatasi Masalah Diabetes? Anda bisa memanfaatkan bahan-bahan herbal seperti:</p>
					<div class="blog_w3l-5">
            <div class="blog_w3l-8">
              <h6>1. BAWANG DAYAK</h6>
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('web/images/elplus/elplus3.png')}}" alt="news image" class="img-fluid">
                </div>
                <div class="col-md-8">
                  <p style="text-align:justify;font-size:20px;">Nama bawang dayak diberikan berdasarkan nama Suku Dayak yang merupakan salah satu suku yang ada di Kalimantan. Suku Dayak mengambil manfaat bawang dayak untuk mengobati berbagai penyakit.</p>
                  <p style="text-align:justify;font-size:20px;">Bawang dayak mampu mengontrol kadar gula dalam darah. Beberapa senyawa dalam bawang dayak yang berperan untuk menurunkan kadar gula darah antara lain adalah flavonoid, glikosida, alkaloid, dan juga saponin. Kegunaan bawang dayak yang satu ini membuat bawang dayak cocok untuk dijadikan salah satu obat alternatif untuk penderita diabetes.</p>

                </div>
              </div><br>
              <h6>2. BROTOWALI</h6>
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('web/images/elplus/elplus4.png')}}" alt="news image" class="img-fluid">
                </div>
                <div class="col-md-8">
                  <p style="text-align:justify;font-size:20px;">Brotowali termasuk dalam tanaman obat tradisional yang sudah dimanfaatkan selama bertahun-tahun oleh banyak masyarakat Indonesia untuk menyembuhkan penyakit, salah satunya adalah diabetes.</p>
                  <p style="text-align:justify;font-size:20px;">Penelitian pada hewan dan kultur sel telah menunjukkan bahwa brotowali dapat merangsang produksi insulin dari sel beta yang ada di pankreas. Brotowali juga dapat meningkatkan penyerapan glukosa oleh otot sehingg brotowali dapat membantu mengontrol diabetes.</p>

                </div>
              </div><br>
              <h6>3. SAMBILOTO</h6>
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('web/images/elplus/elplus5.png')}}" alt="news image" class="img-fluid">
                </div>
                <div class="col-md-8">
                  <p style="text-align:justify;font-size:20px;">Sambiloto atau Andrographis paniculata merupakan tanaman yang mempunyai rasa sangat pahit. Sudah lama tanaman ini dimanfaatkan sebagai obat tradisional yang dipercaya mampu menyembuhkan banyak penyakit, salah satunya membantu mencegah diabetes karena dapat menurunkan kadar glukosa.</p>
                </div>
              </div><br>

              <h6>4. BROTOWALI</h6>
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('web/images/elplus/elplus6.png')}}" alt="news image" class="img-fluid">
                </div>
                <div class="col-md-8">
                  <p style="text-align:justify;font-size:20px;">Sarang semut merupakan kumpulan tumbuhan epifit, yaitu tumbuhan yang menumpang hidup di pohon lain, tetapi tidak menyerap makanan dari pohon itu. Kandungan senyawa aktif polifenol yang ada dalam sarang semut berperan sebagai zat antidiabetes.</p>
                  <p style="text-align:justify;font-size:20px;">Kemampuan sarang semut dalam pengobatan diabetes adalah kemampuan obat herbal ini dalam memulihkan fungsi organ tubuh, khususnya pankreas. Dampaknya, tubuh dapat menghasilkan hormon insulin yang cukup. Dengan demikian, gula darah dapat diolah menjadi zat yang mudah diserap oleh tubuh.</p>
                </div>
              </div>

            </div>

					</div>
			</div>
			</div>
		</div>
</section>
<section class="ban_bottom1 py-5" id="more">
	<div class="container py-lg-5">
		<div class="ban_bottom_top text-center py-lg-5">
			<h3>KABAR BAIKNYA ADALAH SAAT INI SUDAH ADA EKSTRAK HERBAL ALAMI TANPA BAHAN KIMIA YANG MEMAKSIMALKAN FUNGSI SEL TUBUH DAN MENCEGAH DIABETES</h3>
		</div>
	</div>
</section>
<section class="blog_w3l py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top" style="line-height: 30px;">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"><center><strong>AKUARINE Luxurious Facial Wash Treatment</strong>  ADALAH <strong>JAWABANYA </strong></center></h2>
        <div class="col-md-12 newsgrid1">
        <center>  <img src="{{asset('web/images/elplus/elplus7.png')}}" width="100%" style="max-width:500px" alt="news image" class="img-fluid"></center>
          <br><br><br>

        </div>

        <h4 class="heading text-left mb-5"><strong>Kandungan serta manfaat dari ELPLUS Komposisi ELPLUS</strong> </h4>
          <!-- <img src="{{asset('web/images/penyebab.jpg')}}" alt="news image" class="img-fluid"> -->
          <div class="blog_w3l-5">
            <h6>Kandungan ELPLUS :</h6>
            <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Tiap (500 mg) kapsul mengandung	:</p>
            <ol type="a" style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
              <li>Bawang Dayak (eleutherine Palmifollia Bulbus Extractum) <strong>200 mg </strong></li>
              <li>Sambiloto (andrographis paniculata folium Extractum) <strong>100 mg</strong></li>
              <li>Sarang Semut (myrmecodia pendans extractum) <strong>100 mg</strong></li>
              <li>Sarang Semut (myrmecodia pendans extractum) <strong>50 mg</strong></li>
              <li>Kencur (kaempferia galangal rhizome extractum) <strong>50 mg</strong></li>
            </ol>
          </div>
          <div class="blog_w3l-5"><br>
            <h6>Manfaat ELPLUS :</h6>
            <ol type="1" style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
              <li>Membantu menurunkan kadar gula darah</li>
              <li>Membantu mengontrol tekanan darah</li>
              <li>Membantu meningkatkan kekebalan tubuh</li>
              <li>Membantu memperbaiki dinding pembuluh darah</li>
              <li>Membantu melindungi sel hati (Liver)</li>
              <li>Membantu mencegah pembentukan sel kanker</li>
              <li>Berfungsi sebagai antibiotik alami</li>
              <li>Membantu mencegah penggumpalan darah</li>
              <li>Membantu meredakan demam dan peradangan</li>
              <li>Membantu mengurangi diare</li>
              <li>Membantu mengurangi nyeri</li>
            </ol>

          </div>
<br>

<br>
      </div>

      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-1.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-2.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-3.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-4.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-5.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-6.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-7.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('web/images/elplus/fungsi-elplus-8.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
      </div>
      <div class="col-md-12">
        <br>
        <center><h2>MAU TERHINDAH DARI PENYAKIT DIABETES SECARA ALAMI ?</h2></center>
        <center> <a href=""><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;"  alt="news image" class="img-fluid"/></a> </center><br>
      </div>
			</div>
			</div>
		</div>
</section>
<section class="contact py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
<h3 class="heading text-center"> <strong> TESTIMONI CHAT</strong> </h3>
<div class="swiper-container">
  <div class="swiper-wrapper">
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-1.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-2.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-3.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-4.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-5.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-6.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-7.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-8.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/elplus/testimoni-elplus-9.jpg')}}" /></div>
  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>


</div>
</div>
</div>
</section>


<!-- <section class="news py-5"> -->
  <div class="container-fluid pb-video-container">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="text-center">TESTIMONI VIDEO</h3>
        <div class="row pb-row">
            <div class="col-md-6 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/J9A00nFG3zU" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Elplus 1</label>
            </div>
            <div class="col-md-6 pb-video">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/NL7hKJBIJg8" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Elplus 2</label>
            </div>
        </div>
        <!-- <div class="row pb-row">
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/UY1bt8ilps4?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">F.O. and Peeva - Lichnata</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/QpbQ4I3Eidg?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Machine Gun - Bad Things</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/h3kRIxLruDs?ecver=" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">INNA - Say it with your body</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/Jr4TMIU9oQ4?ecver=1" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">INNA - Gimme Gimme</label>
            </div>
        </div> -->
    </div>
</div>


<!-- <section> -->
<section class="blog_w3l py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-6 blog_w3l_right col-md-offset-3">
  <center><h2>MAU DAPAT DISKON RESELLER ?</h2></center>
            <!-- <center> <a href="{{url('diskon-reseller')}}"> <button type="button" name="button" style=" height:100px;width:300px;background:rgba(240, 183, 24, 1);font-size:40px;font-weight: bold;">MAU !</button> </a></center><br> -->
            <center> <a href="{{url('elplus/diskon-reseller')}}?ref={{$ids}}"><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>

<hr>
  <center><h2>KONSULTASI VIA WA</h2></center>
            <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konsultasi%20Tentang%20Elplus"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
</div></div></div>
</section>







	<!-- //blog -->
	<!-- contact -->
	<section class="contact py-5" id="contact">
		<div class="container py-lg-5">
			<div class="text-center">
				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
			</div>
			<div class="row contact-top">
				<div class="col-lg-12 contact_grid_right">
					<form action="#" method="post">
						<div class="row contact_top">
							<div class="col-sm-12">
								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
							</div>
							<div class="col-sm-12">
								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
							</div>
              <div class="col-sm-12">
								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
							</div>
              <br><br><br>
              <div class="col-sm-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Kirim Data</button>
    							<button type="reset" class="btn">Bersihkan Form</button>
              </div>
						</div>
							<div class="clearfix"> </div>
					</form>
				</div>
		</div>
	</section>
	 <!-- //contact -->
	 <div class="cpy-right text-center">
		<div class="container">
				<p class="py-md-5 py-4">© COPYRIGHT2019
				</p>
			</div>
	</div>
  <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
  <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js'></script>
  <script >var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
  spaceBetween: 0,
    //loop: true,
  autoplay: 2500,
    autoplayDisableOnInteraction: true,
    slidesPerView: 4,
    coverflow: {
        rotate: 30,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    }
  });

  //# sourceURL=pen.js
  </script>
   </body>
</html>
