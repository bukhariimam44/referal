<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->


      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <link href="{{asset('bootstrap/js/bootstrap.min.css')}}" rel="stylesheet" type="text/javascript" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!-- <link href="{{asset('bootstrap/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all"> -->
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">

    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/abennington/pen/GZeyKr?depth=everything&order=popularity&page=45&q=pack&show_forks=false" />

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css'><link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
  <style class="cp-pen-styles">img { max-height: 100% }
  .swiper-container {
    width: 100%;
    height: 400px;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  /*width:90%;*/
  }
  div{
    margin-bottom: 20;
  }
  /* carousel */
  .media-carousel
  {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
    margin-top: 30px;
  }
  /* Previous button  */
  .media-carousel .carousel-control.left
  {
    left: -12px;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Next button  */
  .media-carousel .carousel-control.right
  {
    right: -12px !important;
    background-image: none;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    height: 40px;
    width : 40px;
    margin-top: 30px
  }
  /* Changes the position of the indicators */
  .media-carousel .carousel-indicators
  {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
  }
  /* Changes the colour of the indicators */
  .media-carousel .carousel-indicators
  {
    background: #c0c0c0;
  }
  .media-carousel .carousel-indicators .active
  {
    background: #333333;
  }
  .media-carousel img
  {
    width: 250px;
    height: 100px
  }
  .parav .li-li li{
     text-align:justify;
     font-size:20px; color:#888;
     font-family: 'Source Sans Pro', sans-serif;"
  }
  /* End carousel */

</style>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: 'Source Sans Pro', sans-serif;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #2c3e50;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }

        .pb-video-frame:hover {
            height: 300px;
        }

    .pb-row {
        margin-bottom: 10px;
    }

    .parav .li-li{
       text-align:justify;
       font-size:20px; color:#888;
       font-family: 'Source Sans Pro', sans-serif;"
    }
</style>
</head>
   <body>

	<section class="news py-5" id="about">
		<div class="container py-lg-5">
			<h2 class="heading text-center" style="font-size:30px;">  <strong>MENGABAIKAN KEPUTIHAN DAPAT BERAKIBAT FATAL <br>WASPADA SEBELUM TERLAMBAT
</strong> </h2>

      	<div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/volare/lp-volare-1.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Apa Itu Keputihan?</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Keputihan adalah keluarnya cairan dari alat genitalia wanita yang tidak berupa darah. Keputihan disebut juga dengan istirah leukorrhoea atau fluor albus dan white discharge. Hampir semua wanita akan mengalami dan pernah mengalami kejadian ini. Keputihan ini menimbulkan ketidaknyamanan dan gangguan rasa percaya diri pada wanita bila terlalu berlebihan. Keputihan fisiologis tidak merugikan karena hal ini sangat wajar terjadi tetapi keputihan yang patologis dan berlebihan perlu dicari penyebabnya karena dapat menimbulkan komplikasi.</p>
                </div>
		</div>
	</section>

<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/volare/lp-volare-2.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Keputihan Seperti Susu Basi</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Keputihan seperti susu basi yang berwujud cairan atau adanya rasa basah yang berasal dari alat kelamin wanita adalah abnormal. Perubahan dalam jumlah yang lebih besar, seperti warna atau bau dan cairan yang keluar dari alat kelamin wanita dan terkadang mengalami adanya infeksi. Adanya rasa gatal pada alat kelamin dapat memiliki banyak penyebab.</p>
                </div>
			</div>
			</div>
		</div>
</section>
<section class="news py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Penyebab Umum Keputihan Abnormal Seperti Susu Basi</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Keputihan abnormal seperti susu basi dapat menjadi tanda adanya infeksi jamur atau vaginosis bakteri yang tidak ditularkan secara seksual atau adanya tanda trichomononas, gonore (kencing nanah), dan klamidia, yang semuanya ditularkan secara seksual. Tanda-tanda atau ciri-ciri lain dari infeksi ini ditentukan dari bagaimana cara perawatannya.</p>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/volare/lp-volare-3.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Masalah Yang Ditimbulkan Akibat Keputihan Abnormal</strong></center> </h2>

          <div class="blog_w3l-5">
						<!-- <h6>1. Perubahan Hormon.</h6> -->
						<p style="text-align:justify;font-size:20px;">Keputihan pada dasarnya dapat digolongkan menjadi 2 golongan, yaitu keputihan normal (fisiologis) dan keputihan abnormal (patologis). Keputihan patologis dapat disebabkan oleh infeksi yang biasanya disertai dengan adanya rasa gatal di dalam vagina dan di sekitar bibir vagina bagian luar. Yang sering menimbulkan keputihan adalah bakteri, virus, jamur atau juga parasit. Infeksi ini dapat menjalar dan menimbulkan peradangan ke saluran kencing sehingga menimbulkan rasa pedih saat penderita buang air kecil.</p>
                </div>
			</div>
			</div>
		</div>
</section>
<section class="news py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
				<h2 class="heading text-center" style="font-size:30px;"> <center> <strong>PENYEBAB KEPUTIHAN SECARA UMUM</strong></center> </h2>

          <div class="blog_w3l-5">
            <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
              <li>Pemakaian antibiotik/steroid yang terlalu lama -> Bakteri “baik” penjaga pH vagina mati -> Jamur tumbuh subur</li>
              <li>Infeksi jamur, bakteri, protozoa</li>
              <li>Sabun pencuci vagina yang tidak tepat -> Keseimbangan pH vagina terganggu</li>
              <li>Pil KB -> Keseimbangan hormon terganggu</li>
              <li>Hormon yang tidak seimbang</li>
              <li>Memakai tisu berklorin saat mencuci organ intim</li>
              <li>Terlalu sering menggunakan toilet umum yang kotor</li>
              <li>Pemakaian bedak di daerah organ intim</li>
              <li>Menggunakan celana dalam berbahan nilon</li>
              <li>Sering memakai celana jins yang ketat</li>
              <li>Jarang ganti pembalut saat haid</li>
            </ol>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="services py-5" id="blog">
	<div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
        <div class="row news-grids py-lg-5 mt-3 text-center">
						<div class="col-md-12 newsgrid1">
							<img src="{{asset('web/images/volare/lp-volare-4.png')}}" alt="news image" class="img-fluid">
            </div>

				</div>
        <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Cara Mencegah Agar Keputihan Tidak Berulang:</strong></center> </h2>

          <div class="blog_w3l-5">
						<ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
              <li>Pola hidup sehat dengan diet yang seimbang, olahraga rutin, istirahat yang cukup, hindari merokok dan alkohol serta hindari stres yang berkepanjangan</li>
              <li>Hanya berhubungan dengan 1 pasangan seksual atau gunakan pengaman atau kondom untuk mencegah penularan penyakit menular seksual (PMS)</li>
              <li>Selalu jaga kebersihan daerah kelamin dengan menjaganya agar tetap kering dan tidak lembab dan selalu bersih.</li>
              <li>Biasakan membasuh dengan cara yang benar saat sehabis buang air kecil (BAK atau kencing) atau buang air besar (BAB) yaitu dari depan ke belakang</li>
              <li>Penggunaan cairan pembersih untuk vagina sebaiknya tidak berlebihan karena dapat mematikan flora normal vagina. Jika perlu, lakukan konsultasi terlebih dahulu dengan dokter.</li>
              <li>Hindari semua produk berbahan kimia yang dapat menyebabkan iritasi</li>
              <li>Hindari pemakaian barang-barang yang memudahkan penularan seperti meminjam perlengkapan mandi dan sebagainya</li>
              <li>Hindari berendam air panas karena jamur dapat menyebabkan keputihan tumbuh di kondisi yang hangat</li>
              <li>Hindari menggaruk vagina</li>
            </ol>
          </div>
			</div>
			</div>
		</div>
</section>
<section class="ban_bottom1 py-5" id="more">
	<div class="container py-lg-5">
		<div class="ban_bottom_top text-center py-lg-5">
			<h3>Nah, tahukah anda solusi praktis dan alami tanpa bahan kimia
yang bisa membantu mengatasi masalah keputihan?</h3>
		</div>
	</div>
</section>
<section class="news py-5" id="about">
  <div class="container py-lg-5">
    <h2 class="heading text-center" style="font-size:30px;">  <strong>Manfaat Buah Majakani untuk Kesehatan Organ Intim Wanita
</strong> </h2>

      <div class="row news-grids py-lg-5 mt-3 text-center">
          <div class="col-md-12 newsgrid1">
            <img src="{{asset('web/images/volare/lp-volare-5.png')}}" alt="news image" class="img-fluid">
          </div>

      </div>
      <h2 class="heading text-center" style="font-size:30px;"> <center> <strong>Apa Itu Keputihan?</strong></center> </h2>

        <div class="blog_w3l-5">
          <!-- <h6>1. Perubahan Hormon.</h6> -->
          <p style="text-align:justify;font-size:20px;">Majakani dipercaya memiliki khasiat begitu besar bila dikonsumsi oleh para wanita. Dengan kandungan-kandungan baiknya, seperti karbohidrat, vitamin C, vitamin A, antioksidan, asam tanat, serat, kalsium, tannin, zat besi dan protein, buah yang asalnya dari pohon majakani ini akan makin efektif dalam menjaga kesehatan tubuh.</p>
          <p style="text-align:justify;font-size:20px;">Berikut adalah manfaat buah majakani :</p>
          <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
            <li> <strong>Merawat Organ Intim Wanita</strong> </li>
            <pstyle="text-align:justify;font-size:20px;">Dalam penggunaan dengan cara tradisional, buah majakani ini diolah dengan ditumbuk untuk menghaluskannya dan hasil tumbukan yang sudah benar-benar lembut itulah yang digunakan untuk merawat organ intim wanita. Cara perawatannya cukup mudah karena hanya tinggal mengoleskan tumbukan tersebut ke bagian organ kewanitaan.</p>
            <li> <strong>Kepuasan Pasangan Suami Istri</strong> </li>
            <pstyle="text-align:justify;font-size:20px;">Dengan elastisitas organ intim wanita yang meningkat dengan menggunakan buah majakani, otomatis tidak hanya wanita saja yang merasa senang. Bagi yang sudah menikah, tentu hal ini menjadi kepuasan bersama, yaitu bagi istri dan juga sang suami. Saat berhubungan intim akan terasa manfaatnya bagi kedua pihak. Ini dikarenakan buah majakani sudah terkenal sejak dari dulu kala akan manfaatnya yang bisa membuat organ wanita lebih kencang dan elastis.</p>
            <li> <strong>Membantu Membersihkan Bakteri dan Jamur</strong> </li>
            <pstyle="text-align:justify;font-size:20px;">Bila area organ intim wanita tidak dirawat dengan baik, bakteri dan jamur akan dapat menyerangnya. Maka, dengan buah majakani, bakteri dan jamur yang terdapat di bagian organ intim akan mampu diatasi, termasuk cairan berlebihan yang kemungkinan ada di sekitar organ intim pun akan dihilangkan. Selain membersihkan daerah organ intim supaya lebih terasa nyaman bagi wanita, kerapatan organ vital tersebut pun akan mampu ditingkatkan sehingga wanita akan merasakan kepuasan tersendiri.</p>
            <li> <strong>Menyembuhkan Luka</strong> </li>
            <pstyle="text-align:justify;font-size:20px;">Karena kepopulerannya lebih tinggi di kalangan wanita yang sudah menikah, maka tidak heran juga bila buah ini pada umumnya dimanfaatkan sebagai penyembuh luka pasca bersalin. Para wanita yang telah mengalami proses bersalin pun mampu memanfaatkan buah ini demi mengembalikan keadaan otot rahim yang telah mengendur untuk menjaga kondisi rahim tetap baik.</p>
          </ol>
        </div>
  </div>
</section>

<section class="services py-5" id="services">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top" style="line-height: 30px;">
			<div class="col-lg-12 blog_w3l_right">
        <h2 class="heading text-center" style="font-size:30px;">  <strong>VOLARE FEMININE HYGIENE TISSUE ADALAH TISU KHUSUS ORGAN INTIM WANITA
    YANG MENGANDUNG BUAH MAJAKANI DAN CHAMOMILE</strong> </h2>
					<center><img src="{{asset('web/images/volare/lp-volare-6.png')}}" alt="news image" class="img-fluid"></center><br><br>
          <!-- <h4 class="heading text-left mb-5"><center><strong>BIOCE SILVER SPRAY</strong></center></h4> -->
          <!-- <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;"><strong>BIOCE SILVER SPRAY</strong> adalah air ionic (air perak murni). Ion Perak Ag+ atau Ion Silver adalah air tanpa bau, tawar, tidak pedas, tidak berbahaya untuk mata, tidak ada radikal bebas, tidak berbahaya untuk manusia dan tidak memiliki reaksi dengan obat lainnya. Ion perak meningkatkan pencernaan, membantu regenerasi sel yang rusak, membantu mencegah demam, flu, dan penyakit yang disebabkan organisme, baik bakteri, jamur, parasit maupun virus.</p> -->
          <!-- <p style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">Penggunaan ion perak tidak membahayakan bakteri baik di dalam pencernaan. Oleh karena itu, ion perak tidak menimbulkan mual atau tumbuhnya jamur yang biasanya terlihat pada penggunaan antibiotik farmasi. Ion Perak Ag+ Membunuh dan Melawan Virus, Bakteri, Parasit, & Jamur hingga 650 jenis dalam waktu 6 Menit! Antibiotik hanya mampu membunuh 7 jenis saja.</p> -->
					<div class="blog_w3l-5">
              <h6>Kandungan serta dari Volare
Komposisi Volare :</h6>
              <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
                <li>Water</li>
                <li>cocamidopropyl betaine</li>
                <li>Peg 40 Hydrogenated castor oil</li>
                <li>Piper betle Extract</li>
                <li>Cocamide Dea</li>
                <li>Fragrance</li>
                <li>Majakani Extract</li>
                <li>Citric Acid</li>
                <li>Methyloroisothiazolinano</li>
                <li>Methylisotthiazolinano</li>
              </ol>

            </div>
            <div class="blog_w3l-5">
                <h6>Manfaat Tissue Volare (Feminine Hygiene Tissue) dengan formula Chamomile :</h6>
                <ol style="text-align:justify;font-size:20px; color:#888;font-family: 'Source Sans Pro', sans-serif;">
                  <li>Mengetatkan otot organ intim wanita</li>
                  <li>Memberikan rasa kesat di organ kewanitaan</li>
                  <li>Menstabilkan PH asam & mengurangi cairan (basah) berlebihan</li>
                  <li>Membuat aroma lebih wangi disekitar organ intim</li>
                  <li>Membantu mengencangkan otot-otot perut & rahim setelah melahirkan</li>
                  <li>Membina rahim & membersihkan selepas bersalin & haid</li>
                  <li>Mencegah Keputihan</li>
                  <li>Menghilangkan gatal-gatal pada organ intim</li>
                  <li>Mencegah jamur yang menyebabkan bau tak sedap</li>
                  <li>Menjaga agar kulit sekitar organ intim tetap sehat dan terawat.</li>
                  <li>Mencegah Kanker Serviks</li>
                  <li>Menghaluskan & melembutkan kulit area organ kewanitaan</li>
                  <li>Merileksasikan otot kewanitaan</li>
                  <li>Mengatasi KRAM saat menstruasi</li>
                </ol>

              </div>
<br>
					</div>
          <div class="col-md-3" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/volare/fungsi-volare-1.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-3" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/volare/fungsi-volare-2.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-3" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/volare/fungsi-volare-3.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-3" style="margin-bottom: 20px;">
            <img src="{{asset('web/images/volare/fungsi-volare-4.jpg')}}" width="100%"  alt="news image" class="img-fluid"/>
          </div>
          <div class="col-md-12">
            <center><h2>MAU CEGAH KEPUTIHAN DENGAN CARA
YANG PRAKTIS DAN ALAMI</h2></center>
            <center> <a href=""><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>
          </div>
			</div>
		</div>
</section>

<section class="contact py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-12 blog_w3l_right">
<h3 class="heading text-center"> <strong> TESTIMONI CHAT</strong> </h3>
<div class="swiper-container">
  <div class="swiper-wrapper">
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-1.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-2.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-3.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-4.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-5.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-6.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-7.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-8.jpg')}}" /></div>
      <div class="swiper-slide"><img src="{{asset('web/images/volare/testimoni-chat-volare-9.jpg')}}" /></div>
  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>


</div>
</div>
</div>
</section>


<!-- <section class="news py-5"> -->
  <div class="container-fluid pb-video-container">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="text-center">TESTIMONI VIDEO</h3>
        <div class="row pb-row">
            <div class="col-md-6 pb-video  col-md-offset-3">
                <iframe width="100%" height="230" src="https://www.youtube.com/embed/JwfFveGC_PQ" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Volare 1</label>
            </div>
            <!-- <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Volare 2</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Volare 3</label>
            </div>
            <div class="col-md-3 pb-video">
                <iframe width="100%" height="230" src="" frameborder="0" allowfullscreen></iframe>
                <label class="form-control label-warning text-center">Testimoni Produk Volare 4</label>
            </div> -->

        </div>
    </div>
</div>


<!-- <section> -->
<section class="blog_w3l py-5">
  <div class="container py-lg-5">
		<div class="row blog_w3l_top">
			<div class="col-lg-6 blog_w3l_right col-md-offset-3">
  <center><h2>MAU DAPAT DISKON RESELLER ?</h2></center>
  <center> <a href="{{url('volare/diskon-reseller')}}?ref={{$ids}}"><img src="{{asset('web/images/mau.png')}}" width="80%" style="max-width:250px;" alt="news image" class="img-fluid"/></a> </center><br>

<hr>
  <center><h2>KONSULTASI VIA WA</h2></center>
            <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konsultasi%20Tentang%20Volare"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
</div></div></div>
</section>







	<!-- //blog -->
	<!-- contact -->
	<section class="contact py-5" id="contact">
		<div class="container py-lg-5">
			<div class="text-center">
				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
			</div>
			<div class="row contact-top">
				<div class="col-lg-12 contact_grid_right">
					<form action="#" method="post">
						<div class="row contact_top">
							<div class="col-sm-12">
								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
							</div>
							<div class="col-sm-12">
								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
							</div>
              <div class="col-sm-12">
								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
							</div>
              <br><br><br>
              <div class="col-sm-12" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Kirim Data</button>
    							<button type="reset" class="btn">Bersihkan Form</button>
              </div>
						</div>
							<div class="clearfix"> </div>
					</form>
				</div>
		</div>
	</section>
	 <!-- //contact -->
	 <div class="cpy-right text-center">
		<div class="container">
				<p class="py-md-5 py-4">© COPYRIGHT2019
				</p>
			</div>
	</div>
  <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
  <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
  <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js'></script>
  <script >var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
  spaceBetween: 0,
    //loop: true,
  autoplay: 2500,
    autoplayDisableOnInteraction: true,
    slidesPerView: 4,
    coverflow: {
        rotate: 30,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    }
  });

  //# sourceURL=pen.js
  </script>
   </body>
</html>
