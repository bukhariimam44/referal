<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>BLA BLA</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Unified Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
      <!-- <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);

         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script> -->
      <style>
      .button {
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    padding: 12px 24px;
    border: 1px solid #a12727;
    border-radius: 8px;
    background: #ff4a4a;
    background: -moz-linear-gradient(top, #ff4a4a, #992727);
    background: linear-gradient(to bottom, #ff4a4a, #992727);
    text-shadow: #591717 1px 1px 1px;
    font: normal normal bold 20px arial;
    color: #ffffff;
    text-decoration: none;
}
.button:hover,
.button:focus {
    background: #ff5959;
    background: -moz-linear-gradient(top, #ff5959, #b62f2f);
    background: linear-gradient(to bottom, #ff5959, #b62f2f);
    color: #ffffff;
    text-decoration: none;
}
.button:active {
    background: #982727;
    background: -moz-linear-gradient(top, #982727, #982727);
    background: linear-gradient(to bottom, #982727, #982727);
}
.button:before{
    content:  "\0000a0";
    display: inline-block;
    height: 24px;
    width: 24px;
    line-height: 24px;
    margin: 0 4px -6px -4px;
    position: relative;
    top: 0px;
    left: 0px;

    background-size: 100% 100%; */
}

      </style>
      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{asset('web/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="{{asset('web/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
	  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
</head>
   <body>
     <section class="news py-5">
     	<div class="container py-lg-5">
     		<div class="row blog_w3l_top">
     			<div class="col-lg-12 blog_w3l_right">
     				<h2 class="heading text-center" style="font-size:30px;"> <strong>Mau punya bisnis kekinian dan memiliki keuntungan besar?
Dibimbing oleh tenaga ahlinya dan dijalankan secara online?</strong>
   </h2>
     				<center>	<img src="{{asset('web/images/volare/reseller-volare-1.png')}}" alt="news image" class="img-fluid"></center>
     					<div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>Apakah kamu ingin berbisnis namun memiliki masalah-masalah seperti di bawah ini:​</h6>
                   <ol style="text-align:justify;font-size:20px;color:#888;">
                     <li>Belum Tau Harus Mulai Dari Mana ?</li>
                     <li>Belum Memiliki Produk ?</li>
                     <li>Modal Terbatas ?</li>
                     <li>Hanya Memiliki Sedikit Waktu ?</li>
                     <li>Belum Punya Mentor & Komunitas ?</li>
                     <li>Tidak Jago Jualan ?</li>
                     <li>Masih Gaptek/ Tidak Mengerti Cara Jualan Online ?</li>
                     <li>Sudah Pernah Bisnis Tapi Hasilnya Kurang Memuaskan (Profit Kecil, Support Kurang, Tidak Diajarkan Digital Marketing, Produknya Kurang Laku)?</li>
                   </ol>
                </div>

     					</div>
     			</div>


     			</div>
     		</div>
     </section>
     <section class="ban_bottom1 py-5" id="more">
       <div class="container py-lg-5">
         <div class="ban_bottom_top text-center py-lg-5">
           <h3>Tenang saja, dengan menjadi reseller KAMI semua kendala di atas akan teratasi!</h3>
         </div>
       </div>
     </section>
     <section class=" py-5" id="services">
     	<div class="container py-lg-5">
     		<div class="row blog_w3l_top">
     			<div class="col-lg-12 blog_w3l_right">
     				<h4 class="heading text-left mb-5"><strong>Keuntungan Menjadi Reseller KAMI?</strong></h4>

     					<div class="blog_w3l-5">
                 <div class="blog_w3l-8">
                   <h6>1. HARGA PRODUK LEBIH BERSAHABAT</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-2.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                   <p style="text-align:justify;font-size:20px;">Produknya bisa jauh lebih BERSAHABAT dan produknya tetap BERKUALITAS TINGGI. Bila kamu ingin menjual produk sebanyak-banyaknya, otomatis kamu perlu memikirkan keuangan rata-rata konsumen kamu bukan?</p>
                   <p style="text-align:justify;font-size:20px;">Itulah yang membuat VOLARE jauh lebih bersahabat dengan reseller atau konsumen. VOLARE menggunakan sistem Reseller, yang memungkinkan kamu untuk mendapatkan penghasilan tanpa banyak syarat.</p>
                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>2. MARKET YANG LUAS & PRODUK LAKU DI PASARAN</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-3.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                  <p style="text-align:justify;font-size:20px;">Bisnis di bidang kesehatan adalah bisnis yang tidak pernah mati dan akan tumbuh semakin besar setiap tahunnya. Apalagi produk VOLARE ini sudah terbukti bisa membantu kesehatan dengan cara yang sangat PRAKTIS & ALAMI. Target market kamu bisa mulai dari Anak Muda, Ibu Rumah Tangga, Karyawan yang sibuk di kantor, Pensiunan, dan masih banyak lagi.</p>
                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>3. RIBUAN KONTEN UPDATE & MATERI IKLAN (FOTO/BANNER, VIDEO, COPYWRITING) GRATIS UNTUK SARANA PROMOSI</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-4.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                   <p style="text-align:justify;font-size:20px;">Satu hal yang sangat diperlukan sebelum memulai berbisnis adalah kamu wajib memiliki konten yang TEPAT & MENARIK. Nah, kamu perlu tahu bahwa "manusia itu tidak suka dijualin", jadi apabila kamu ingin membuat konten, jangan buat secara HARD SELLING (jualan banget), tapi buat lah konten yang SOFT SELLING (memberikan informasi & edukatif).</p>
                   <p style="text-align:justify;font-size:20px;">Nah, masalahnya untuk membuat konten seperti kriteria di atas, itu tidak mudah. Kalau kamu membuat konten sendiri, itu akan menguras cukup banyak waktu & pikiran, kamu perlu membayar content creator (jasa khusus pembuat konten).</p>
                   <p style="text-align:justify;font-size:20px;">Dan... masalahnya lagi, untuk mencari jasa pembuat konten yang sesuai dengan kriteria di atas tidaklah mudah, andai ada pun kamu perlu mengeluarkan budget yang cukup besar setiap bulannya (Rp 5-10 juta/bulan).</p>
                   <p style="text-align:justify;font-size:20px;">Oleh karena itu, VOLARE sudah menyediakan ribuan konten & materi iklan yang SIAP PAKAI. Jadi, kamu sudah disiapkan "alat perang" untuk berjualan. Kamu tidak perlu repot-repot bikin konten, harus bayar Rp 5-10 juta /bulan. Dengan menjadi reseller Teh Anugerah, kamu hanya tinggal belajar, praktek, lalu COPY PASTE saja untuk mendapatkan keuntungan. Asyik sekali bukan?</p>
                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>4. PROFIT BESAR & POTENSI BANGUN BISNIS</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-5.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                   <p style="text-align:justify;font-size:20px;">Kamu bisa mendapatkan profit bersih 25-40% atau Rp 45.000 s.d Rp 150.000 per produk. Misalnya kamu HANYA bisa menjual 3 produk/hari, penghasilan tambahan kamu per bulan bisa mencapai Rp 4.050.000 – Rp 12.500.000.</p>
                   <p style="text-align:justify;font-size:20px;">Bayangkan bila kamu bisa menjual lebih dari 5-10 produk / hari, penghasilan kamu pasti akan jauh lebih besar lagi. Apalagi semuanya bisa kamu lakukan secara online dari rumah saja (semua caranya akan diajarkan detail), menarik bukan?</p>
                   <p style="text-align:justify;font-size:20px;">Selain berjualan, kamu juga bisa benar-benar membangun BISNIS dengan memiliki banyak reseller yang akan membantu meningkatkan penjualan kamu, semuanya akan diajarkan detail. Bisa dijalankan secara offline maupun online.</p>
                   <p style="text-align:justify;font-size:20px;">Bahkan, kamu bisa naik tingkat dari reseller menjadi associate untuk mendapatkan keuntungan-keuntungan yang lain dari Harga Reseller.</p>
                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>5. SISTEM DROPSHIP</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-6.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                   <p style="text-align:justify;font-size:20px;">Kamu bisa menjual produk VOLARE dengan sistem dropship secara online. Jadi kamu tidak perlu keluar modal lagi untuk biaya transportasi, packing & pengiriman barang. MEMUDAHKAN SEKALI!</p>

                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>6. DUKUNGAN PENUH LEWAT SISTEM ONLINE & OFFLINE</h6>
                   <div class="row">
                     <div class="col-md-5">
                       <img src="{{asset('web/images/volare/reseller-volare-7.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                     </div>
                   </div><br>
                   <p style="text-align:justify;font-size:20px;">Selain mendapatkan fasilitas website yang bisa kamu akses full di mana pun & kapan pun selama 24 jam, kamu juga akan mendapatkan bimbingan khusus secara online maupun offline. Kamu akan mendapatkan informasi yang terus update melalui channel Telegram Bank Data SAP. Apabila kamu sudah mempelajari semua tutorial & materinya sampai selesai namun masih ada yang belum dimengerti, tenang saja, Kamu bisa langsung KONSULTASI & TANYA JAWAB dengan admin kami sampai kamu bisa betul-betul praktek.</p>
                   <p style="text-align:justify;font-size:20px;">Selain dibimbing melalui sistem online, kamu juga akan mendapatkan bimbingan dari sisi offline.</p>

                 </div>
                 <br>
                 <div class="blog_w3l-5">
                   <h6>7. FASILITAS ISTIMEWA! GRATIS UNTUK BELAJAR DIGITAL MARKETING DI CBO ACADEMY</h6>
                   <!-- <img src="{{asset('web/images/cbo.png')}}" alt="Dukungan" class="img-fluid"> <br><br> -->
                   <p style="text-align:justify;font-size:20px;">Kamu akan mendapatkan FASILITAS ISTIMEWA dengan mendapat akses ke CBO Academy secara GRATIS. Apa itu CBO Academy? Sederhananya, kamu bisa belajar segala hal yang berhubungan dengan Digital Marketing di CBO Academy, sehingga kamu mendapatkan penghasilan lewat dunia online.</p>
                   <p style="text-align:justify;font-size:20px;">Jika di luar sana, kamu perlu mengeluarkan belasan – puluhan juta untuk belajar Digital Marketing lengkap, dengan menjadi reseller Teh Anugerah, kamu akan mendapatkan semua fasilitas di CBO Academy secara GRATIS!</p>
<br><br>
                   <ol type="a" style="text-align:justify;font-size:20px;">
                     <li>Problem Slide Story</li>
                     <div class="row">
                       <div class="col-md-5">
                         <img src="{{asset('web/images/akuarine/Problem-Slide-Story.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                       </div>
                     </div>

                     <br><br>
                     <li>Brosur Digital</li>
                     <div class="row">
                       <div class="col-md-5">
                         <img src="{{asset('web/images/akuarine/Brosur-Digital.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                       </div>
                     </div>
                     <br><br>
                     <li>Manfaat produk (slide story)</li>
                     <div class="row">
                       <div class="col-md-5">
                         <img src="{{asset('web/images/akuarine/Manfaat-produk.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                       </div>
                     </div>
                     <br><br>
                     <li>Video Iklan dll</li>
                     <div class="row">
                       <div class="col-md-5">
                         <img src="{{asset('web/images/akuarine/Video-Iklan.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                       </div>
                     </div>
                     <br><br>
                     <li>Testimoni Produk</li>
                     <div class="row">
                       <div class="col-md-5">
                         <img src="{{asset('web/images/akuarine/Testimoni-Produk.png')}}" width="100%" alt="Problem Slide Story" class="img-fluid">
                       </div>
                     </div>
                   </ol>
                 </div>
     					</div>
     			</div>

     			<!-- <div class="col-lg-4 blog_w3l_left">
     				<div class="blog_w3l_left1">
     					<div class="blog_w3l-2">
                 <hr>
     						<h5><center><strong>AKUARINE LUXURIOUS FACIAL TREATMENT</strong></center></h5><br>
                <img src="{{asset('web/images/akuarine.jpg')}}" class="responsive" width="100%"/><br><br>
                <center><img src="{{asset('web/images/klikdisini.gif')}}" class="responsive" width="60%"/></center><br><br>
                <br><br>
                </div>
     				</div> -->
     				<!-- <div class="blog_w3l_left2">
     					<div class="blog_w3l-2">
     						<h5>20 December 2018</h5>
     						<h3 class="mt-2">pellentesque mi non</h3>
     						<p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
     					</div>
     				</div> -->
     				<!-- <div class="blog_w3l_left3">
     					<div class="blog_w3l-2">
     						<h5>20 December 2018</h5>
     						<h3 class="mt-2">pellentesque mi non</h3>
     						<p class="mt-3">Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. pellentesque mi non laoreet eleifend porttitor mollisar</p>
     					</div>
     				</div> -->
     			<!-- </div> -->
     			</div>
     		</div>
     </section>
     <section class="contact py-5" id="contact">
       <div class="container py-lg-5">
     		<div class="row blog_w3l_top">
     			<div class="col-lg-12 blog_w3l_right col-md-offset-1">

       <center><h2>KONSULTASI VIA WA</h2></center>
                 <center> <a href="https://api.whatsapp.com/send?phone=6281314020081&text=Halo%20Admin%20Saya%20Mau%20Konfirmasi"><img src="{{asset('web/images/wa.gif')}}" width="100%" style="max-width:270px;" alt="news image" class="img-fluid"/></a> </center><br><br>
     </div></div></div>
     </section>
     <!-- contact -->
     <section class="contact py-5" id="contact">
   		<div class="container py-lg-5">
   			<div class="text-center">
   				<h3 class="heading text-center">Setelah Anda mengisi data nama & email dengan benar, Anda akan mendapatkan kesempatan Diskon Reseller dari kami & Gratis !</h3>
   			</div>
   			<div class="row contact-top">
   				<div class="col-lg-12 contact_grid_right">
   					<form action="#" method="post">
   						<div class="row contact_top">
   							<div class="col-sm-12">
   								<input type="text" name="Name" placeholder="Name" required="" style="text-align:center;"/>
   							</div>
   							<div class="col-sm-12">
   								<input type="email" name="Email" placeholder="Email" required="" style="text-align:center;">
   							</div>
                 <div class="col-sm-12">
   								<p style="text-align:center;">*Data yang Anda berikan kami jamin kerahasiaannya 100% aman.</p>
   							</div>
                 <br><br><br>
                 <div class="col-sm-12" style="text-align:center;">
                   <button type="submit" class="btn btn-primary">Kirim Data</button>
       							<button type="reset" class="btn">Bersihkan Form</button>
                 </div>
   						</div>
   							<div class="clearfix"> </div>
   					</form>
   				</div>
   		</div>
   	</section>

   	 <!-- //contact -->
   	 <!-- <div class="cpy-right text-center">
   		<div class="container">
   				<p class="py-md-5 py-4">© 2019 All rights reserved | Design by
   					<a> Infiniqa
   				</p>
   			</div>
   	</div> -->
      </body>
   </html>
