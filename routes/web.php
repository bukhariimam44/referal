<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AkuarineController@share');

//AKURINE
Route::get('akuarine', 'AkuarineController@share');
Route::get('akuarine/diskon-reseller','AkuarineController@discon');
//ALUZE
Route::get('aluze', 'AluzeController@share');
Route::get('aluze/diskon-reseller','AluzeController@discon');
//Elplus
Route::get('elplus', 'ElplusController@share');
Route::get('elplus/diskon-reseller','ElplusController@discon');
//BIOCE
Route::get('bioce', 'BioceController@share');
Route::get('bioce/diskon-reseller','BioceController@discon');
//FOLARE
Route::get('volare', 'VolareController@share');
Route::get('volare/diskon-reseller','VolareController@discon');
//TEH ANUGERAH
Route::get('teh-anugerah', 'TehAnugerahController@share');
Route::get('teh-anugerah/diskon-reseller','TehAnugerahController@discon');
Auth::routes();
