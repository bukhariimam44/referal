<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class TehAnugerahController extends Controller
{
  public function share(Request $request)
  {
    Session::put('userid', $request->ref);
    $ids = $request->ref;
      return view('teh_anugerah.index',compact('ids'));
  }
  public function discon(Request $request){
    return view('teh_anugerah.reseller');
  }
}
