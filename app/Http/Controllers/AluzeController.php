<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class AluzeController extends Controller
{
  public function share(Request $request)
  {
    Session::put('userid', $request->ref);
    $ids = $request->ref;
      return view('aluze.index',compact('ids'));
  }
  public function discon(Request $request){
    return view('aluze.reseller');
  }
}
