<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    public function share(Request $request)
    {
      Session::put('userid', $request->ref);
      $ids = $request->ref;
        return view('welcome',compact('ids'));
    }
    public function discon(Request $request){
      return view('reseller');
    }
}
