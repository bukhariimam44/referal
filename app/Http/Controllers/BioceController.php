<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class BioceController extends Controller
{
  public function share(Request $request)
  {
    Session::put('userid', $request->ref);
    $ids = $request->ref;
      return view('bioce.index',compact('ids'));
  }
  public function discon(Request $request){
    return view('bioce.reseller');
  }
}
